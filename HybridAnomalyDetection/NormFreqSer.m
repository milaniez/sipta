function NormedFreqSer = NormFreqSer(FreqSer)
    NormedFreqSer = FreqSer*(length(FreqSer) - 1)/...
        sum(abs(FreqSer(2:length(FreqSer))));
end