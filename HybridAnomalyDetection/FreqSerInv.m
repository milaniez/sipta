function FreqSerOut = FreqSerInv(FreqSerIn)
    FreqSerOut = FreqSerIn;
    FreqSerOut.Mag = FreqSerOut.Mag.^(-1);
end