function AnalyzeResults = ...
        AnalyzeDC(Type,Config,TrainData,ChNames,ChHasLen,ChDCs)
    if strcmp(Type,'MinStdRatioSum')
        AnalyzeResults = AnalyzeDCMinStdRatioSum...
            (Config,TrainData,ChNames,ChHasLen,ChDCs);
    else
        error('Invalid option');
    end
end