function [FreqSerOut1,FreqSerOut2] = InterpolateFreqSerPair(FreqSerIn1,FreqSerIn2)
    FreqSerIn1Pat = FreqSerIn1;
    FreqSerIn2Pat = FreqSerIn2;
    FreqSerIn1Pat.Mag(:) = 0;
    FreqSerIn2Pat.Mag(:) = 0;
    FreqSerOut1 = FreqSerAdd(FreqSerIn1,FreqSerIn2Pat);
    FreqSerOut2 = FreqSerAdd(FreqSerIn2,FreqSerIn1Pat);
end