function Results = MultiPeakClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf,NormOp,varargin)
%%  Prepare variables from varargin
    if nargin == 8
        AllTimeSersT = varargin{1};
        AllTimeSersA = varargin{2};
        PeakVerification = true;
    elseif nargin == 6
        PeakVerification = false;
    else
        error('not enough/more than enough arguments');
    end
%%  Initialize
    if PeakVerification
        [TraceCntT,ChCntT] = size(AllTimeSersT);
        [TraceCntA,ChCntA] = size(AllTimeSersA);
        MinChLen = str2double(Conf.Config.MinChLen.Text);
        ChHasLenT = zeros(TraceCntT,ChCntT);
        ChHasLenA = zeros(TraceCntA,ChCntA);
        if MinChLen > 2
            for TraceNo = 1:TraceCntT
                for ChNo = 1:ChCntT
                    if length(AllTimeSersT{TraceNo,ChNo}) >= MinChLen
                        ChHasLenT(TraceNo,ChNo) = 1;
                    end
                end
            end
            for TraceNo = 1:TraceCntA
                for ChNo = 1:ChCntA
                    if length(AllTimeSersA{TraceNo,ChNo}) >= MinChLen
                        ChHasLenA(TraceNo,ChNo) = 1;
                    end
                end
            end
        else
            error ('Invalid Channel Length');
        end
        PermCnt = str2double(Conf.Config.PermuteCnt.Text);
        PermSeed = Conf.Config.PermuteSeed.Text;
        ConfIntv = str2double(Conf.Config.ConfidenceIntv.Text);
        if ~strcmp(PermSeed,'shuffle')
            PermSeed = str2double(PermSeed);
        end
        RndStr = RandStream('mt19937ar','Seed',PermSeed);
    %   Get the values of thresholds
        AllPermPeaksT = zeros(TraceCntT,ChCntT,PermCnt);
        for TraceNo = 1:TraceCnt
            for ChNo = 1:ChCntT
                for PermNo = 1:PermCnt
                    PermutedCh = AllTimeSersT{TraceNo,ChNo}(randperm(RndStr,...
                        length(AllTimeSersT{TraceNo,ChNo})));
                    ChPer = fft(PermutedCh);
                    ChPer = abs(ChPer);
                    ChPer = ChPer.^2;
                    ChPer(1) = [];
                    ChPer = ChPer/(sum(ChPer)/length(ChPer));
                    [PerMaxMag] = max(ChPer);
                    AllPermPeaksT(TraceNo, ChNo, PermNo) = PerMaxMag;
                end
                AllPermPeaksT(TraceNo, ChNo, :) = ...
                    sort(AllPermPeaksT(TraceNo, ChNo, :));
            end
        end
        ThreshPermIdx = floor(PermCnt*ConfIntv);
        AllMinPeaksT = AllPermPeaksT(:,:,ThreshPermIdx);
    else
        [TraceCntT,ChCntT] = size(AllFreqSersT);
        [TraceCntA,ChCntA] = size(AllFreqSersA);
        ChHasLenT = zeros(TraceCntT,ChCntT);
        for TraceNo = 1:TraceCntT
            for ChNo = 1:ChCntT
                if ~isnan(AllFreqSersT{TraceNo,ChNo}.MinDist) && ...
                        AllFreqSersT{TraceNo,ChNo}.MinDist ~= 0
                    ChHasLenT(TraceNo,ChNo) = 1;
                end
            end
        end
        ChHasLenA = zeros(TraceCntA,ChCntA);
        for TraceNo = 1:TraceCntA
            for ChNo = 1:ChCntA
                if ~isnan(AllFreqSersA{TraceNo,ChNo}.MinDist) && ...
                        AllFreqSersA{TraceNo,ChNo}.MinDist ~= 0
                    ChHasLenA(TraceNo,ChNo) = 1;
                end
            end
        end
        PeakRegParam = str2double(Conf.PeakRegParam.Text);
        AllMinPeaksT = ones(TraceCntT,ChCntT)*PeakRegParam;
    end
    
%%   Prepare data for TrainMultiPeak function
    AllFreqSersHT = cell(TraceCntT,ChCntT);
    PeaksMagsHT = cell(TraceCntT,ChCntT);
    PeaksFreqsHT = cell(TraceCntT,ChCntT);
    PeaksWidthsHT = cell(TraceCntT,ChCntT);
    PeaksRegsHT = cell(TraceCntT,ChCntT);
    PeakRegOption = Conf.PeakReg.Text;
    
    if strcmp(PeakRegOption,'OverMean')
        for TraceNo = 1:TraceCntT
            for ChNo = 1:ChCntT
                if ChHasLenT(TraceNo,ChNo) == 1
                    AllFreqSersHT{TraceNo,ChNo}.MinDist = ...
                        AllFreqSersT{TraceNo,ChNo}.MinDist;
%                     This is not the most efficient way to do this
                    RedIdx = (AllFreqSersT{TraceNo,ChNo}.Freq > 0.5);
                    AllFreqSersHT{TraceNo,ChNo}.Freq = ...
                        AllFreqSersT{TraceNo,ChNo}.Freq;
                    AllFreqSersHT{TraceNo,ChNo}.Mag = ...
                        AllFreqSersT{TraceNo,ChNo}.Mag;
                    AllFreqSersHT{TraceNo,ChNo}.Freq(RedIdx) = [];
                    AllFreqSersHT{TraceNo,ChNo}.Mag(RedIdx) = [];
                    AllFreqSersHT{TraceNo,ChNo}.Mag(1) = 0;
                    if strcmp(NormOp,'Norm')
                        G1Idx = ([AllFreqSersHT{TraceNo,ChNo}.Mag; 0] > ...
                            AllMinPeaksT(TraceNo,ChNo));
                        G1IdxDiff = logical(abs(diff(G1Idx)));
                        PeakCnt = sum(G1IdxDiff)/2;
                        PeakRegIdxs = find(G1IdxDiff);
                        PeaksMagsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                        PeaksFreqsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                        PeaksWidthsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                        PeaksRegsHT{TraceNo,ChNo} = zeros(PeakCnt,2);
                        for PeakNo = 1:PeakCnt
                            Idx1 = PeakRegIdxs(2*PeakNo - 1) + 1;
                            Idx2 = PeakRegIdxs(2*PeakNo);
                            PeaksWidthsHT{TraceNo,ChNo}(PeakNo) = Idx2 - Idx1 + 1;
                            [PeaksMagsHT{TraceNo,ChNo}(PeakNo), idx] = ...
                                max(AllFreqSersHT{TraceNo,ChNo}.Mag(Idx1:Idx2));
                            PeaksFreqsHT{TraceNo,ChNo}(PeakNo) = ...
                                AllFreqSersHT{TraceNo,ChNo}.Freq(idx + Idx1 - 1);
                            PeaksRegsHT{TraceNo,ChNo}(PeakNo,:) = [Idx1,Idx2];
                        end
                    else
                        error('Not implemented yet ...');
                    end
                end
            end
        end
    else
        error('Not implemeted yet');
    end
%     Getting parameters for training
    TrainConf = Conf.Train.Config;
    TrainType = Conf.Train.Type.Text;
%     Train for Multi-peak
    if PeakVerification
        
    else
        Results.Train = ...
            TrainMultiPeak(TrainType,TrainConf,...
            ChNamesT,ChHasLenT,PeaksMagsHT,PeaksFreqsHT,PeaksWidthsHT);
    end
%     Prepare data for AnalyzeMultiPeak function
    AllFreqSersHA = cell(TraceCntA,ChCntA);
    PeaksMagsHA = cell(TraceCntA,ChCntA);
    PeaksFreqsHA = cell(TraceCntA,ChCntA);
    PeaksWidthsHA = cell(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ChHasLenA(TraceNo,ChNo) == 1
                AllFreqSersHA{TraceNo,ChNo}.MinDist = ...
                    AllFreqSersA{TraceNo,ChNo}.MinDist;
%                     This is not the most efficient way to do this
                RedIdx = (AllFreqSersA{TraceNo,ChNo}.Freq > 0.5);
                AllFreqSersHA{TraceNo,ChNo}.Freq = ...
                    AllFreqSersA{TraceNo,ChNo}.Freq;
                AllFreqSersHA{TraceNo,ChNo}.Mag = ...
                    AllFreqSersA{TraceNo,ChNo}.Mag;
                AllFreqSersHA{TraceNo,ChNo}.Freq(RedIdx) = [];
                AllFreqSersHA{TraceNo,ChNo}.Mag(RedIdx) = [];
            end
            AllFreqSersHA{TraceNo,ChNo}.Mag(1) = 0;
            if strcmp(NormOp,'Norm')
                G1Idx = ([AllFreqSersHA{TraceNo,ChNo}.Mag; 0] > PeakRegParam);
                G1IdxDiff = logical(abs(diff(G1Idx)));
                PeakCnt = sum(G1IdxDiff)/2;
                PeakRegIdxs = find(G1IdxDiff);
                PeaksMagsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksFreqsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksWidthsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                for PeakNo = 1:PeakCnt
                    Idx1 = PeakRegIdxs(2*PeakNo - 1) + 1;
                    Idx2 = PeakRegIdxs(2*PeakNo);
                    PeaksWidthsHA{TraceNo,ChNo}(PeakNo) = Idx2 - Idx1 + 1;
                    [PeaksMagsHA{TraceNo,ChNo}(PeakNo), idx] = ...
                        max(AllFreqSersHA{TraceNo,ChNo}.Mag(Idx1:Idx2));
                    PeaksFreqsHA{TraceNo,ChNo}(PeakNo) = ...
                        AllFreqSersHA{TraceNo,ChNo}.Freq(idx + Idx1 - 1);
                end
            else
                error('Not implemented yet ...');
            end
        end
    end
%     Get parameters for analyzing
    AnalyzeConf = Conf.Analyze.Config;
    AnalyzeType = Conf.Analyze.Type.Text;
%     Analyze for MultiPeak
    Results.Analyze = ...
        AnalyzeMultiPeak(AnalyzeType,AnalyzeConf,...
        Results.Train,ChNamesA,logical(ChHasLenA),...
        PeaksMagsHA,PeaksFreqsHA,PeaksWidthsHA);
end