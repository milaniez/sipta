clear
ConfFiles = {...
    'Config2.xml';
    'Config3.xml';
    'Config4.xml';
    'Config5-1.xml';
    'Config5-2.xml';
    'Config6-1.xml';
    'Config6-2.xml'};
MatFileName = {...
    'QNX2.mat';
    'QNX3.mat';
    'QNX4.mat';
    'QNX5-1.mat';
    'QNX5-2.mat';
    'QNX6-1.mat';
    'QNX6-2.mat'};
for i = 1:7
    i
    Input = SiPTAInit(ConfFiles{i});
    [Results,Names,TNames,ANames] = SiPTAMain(Input,ConfFiles{i}); %#ok<ASGLU>
    Scores = GetScoresAsMat(Results)'; %#ok<NASGU>
    save(MatFileName{i},'Input','Results','Names','TNames','ANames','Scores');
    clear Input Results Names TNames ANames Scores
end
clear