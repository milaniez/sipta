function [Results,Names,TNames,ANames] = SiPTA (ConfigFile)
    Input = SiPTAInit (ConfigFile);
    [Results,Names,TNames,ANames] = SiPTAMain(Input,ConfigFile);
end