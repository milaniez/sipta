use strict;
use warnings;

my @a = {"foo", "bar", "niloo", "mehdi"};

my $b = [@a];

my @c = @[$b];

print "@c\n";