function AnalyzeResults = ...
    AnalyzeLengthMinStdRatioSum(Config,TrainData,ChNames,ChExists,ChLengths)
    
    [TraceCnt,~] = size(ChExists);
    AnalyzeResults = cell(TraceCnt,1);
    ScoreThreshold = str2num(Config.ScoreThreshold.Text); %#ok<ST2NM>
    TrainNonZeroDataIdx = logical(TrainData.LengthAvg);
    TrainData.ChNames = TrainData.ChNames(TrainNonZeroDataIdx);
    TrainData.LengthAvg = TrainData.LengthAvg(TrainNonZeroDataIdx);
    TrainData.LengthStd = TrainData.LengthStd(TrainNonZeroDataIdx);
    for TraceNo = 1:TraceCnt
        TraceChNames = ChNames(ChExists(TraceNo,:));
        TraceChLengths = ChLengths(TraceNo,ChExists(TraceNo,:));
        TraceLen = sum(TraceChLengths);
        [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
        TrainAvg = TrainData.LengthAvg(TrainChIdx);
        TrainStd = TrainData.LengthStd(TrainChIdx);
        ValidChLengths = TraceChLengths(AnalyzeChIdx);
        Score = sum(abs((ValidChLengths/TraceLen - TrainAvg)./TrainStd))/length(ValidChLengths);
        AnalyzeResults{TraceNo}.Score = Score;
        AnalyzeResults{TraceNo}.Label = (Score > ScoreThreshold);
    end
end