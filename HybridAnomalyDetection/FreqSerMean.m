function FreqSerOut = FreqSerMean(FreqSersIn)
    FreqSerInCnt = length(FreqSersIn);
    FreqSerOut = FreqSersIn{1};
    for FreqSerNo = 2:FreqSerInCnt
        FreqSerOut = FreqSerAdd(FreqSerOut,FreqSersIn{FreqSerNo});
    end
    FreqSerOut.Mag = FreqSerOut.Mag/FreqSerInCnt;
end