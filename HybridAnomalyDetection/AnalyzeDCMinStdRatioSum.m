function AnalyzeResults = ...
    AnalyzeDCMinStdRatioSum(Config,TrainData,ChNames,ChHasLen,ChDCs)
    
    [TraceCnt,~] = size(ChHasLen);
    AnalyzeResults = cell(TraceCnt,1);
    ScoreThreshold = str2num(Config.ScoreThreshold.Text); %#ok<ST2NM>
    for TraceNo = 1:TraceCnt
        TraceChNames = ChNames(ChHasLen(TraceNo,:));
        TraceChDCs = ChDCs(TraceNo,ChHasLen(TraceNo,:));
        [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
        TrainAvg = TrainData.DCAvg(TrainChIdx);
        TrainStd = TrainData.DCStd(TrainChIdx);
        ValidChDCs = TraceChDCs(AnalyzeChIdx);
        Score = sum(abs((ValidChDCs - TrainAvg)./TrainStd))/length(ValidChDCs);
        AnalyzeResults{TraceNo}.Score = Score;
        AnalyzeResults{TraceNo}.Label = (Score > ScoreThreshold);
    end
end