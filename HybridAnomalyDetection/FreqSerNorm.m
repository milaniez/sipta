function FreqSerOut = FreqSerNorm(FreqSer)
    FreqSerOut = FreqSer;
    Area = FreqSerArea(FreqSer);
    FreqSerOut.Mag = FreqSerOut.Mag/Area;
end