use strict;
use warnings;

open (TRACEINFO,"$ARGV[0]"."\/"."trace_info.txt") or die "trace info file not opened\n";
my $TraceCnt = <TRACEINFO>;
close (TRACEINFO);

my @AllSigList = ();
my %AllSigHash = ();
my $AllSigCnt = 0;

for (my $TraceNo = 0; $TraceNo < $TraceCnt; $TraceNo++)
{
	open (SIGINFO, "$ARGV[0]\/trace$TraceNo\/sig_info.txt") or die "siginfo file not opened\n";
	my $SigCnt = <SIGINFO>;
	close (SIGINFO);
	open (SIGLIST, "$ARGV[0]\/trace$TraceNo\/sig_list.txt") or die "siglist not opened\n";
	for (my $SigNo = 0; $SigNo < $SigCnt; $SigNo++)
	{
		my $Sig = <SIGLIST>;
		$Sig =~ /^\w*: *(.*?)\n$/;
		if (! exists $AllSigHash{$1})
		{
			$AllSigHash{$1} = $AllSigCnt;
			push (@AllSigList, $1);
			$AllSigCnt++;
		}
	}
	close (SIGLIST);}

%AllSigHash = ();

for (my $AllSigNo = 0; $AllSigNo < $AllSigCnt; $AllSigNo++)
{
	$AllSigHash{$AllSigList[$AllSigNo]} = $AllSigNo;}
for (my $TraceNo = 0; $TraceNo < $TraceCnt; $TraceNo++)
{
	open (SIGINFO, "$ARGV[0]\/trace$TraceNo\/sig_info.txt") or die "siginfo file not opened\n";
	my $SigCnt = <SIGINFO>;
	close (SIGINFO);
	open (SIGLIST, "$ARGV[0]\/trace$TraceNo\/sig_list.txt") or die "siglist not opened\n";
	for (my $SigNo = 0; $SigNo < $SigCnt; $SigNo++)
	{
		my $Sig = <SIGLIST>;
		$Sig =~ /^\w*: *(.*?)\n$/;
		if (! exists $AllSigHash{$1})
		{
			die "ERROR!!!\n";
		}
		rename ("$ARGV[1]\/trace$TraceNo\/fig$SigNo.jpg","$ARGV[1]\/trace$TraceNo\/F$AllSigHash{$1}-$TraceNo.jpg");
	}
	close (SIGLIST);
}