function AnalyzeResults = AnalyzeMultiPeak2...
        (TrainData,ChNames,ChHasLen,...
        PeaksMags,PeaksFreqs,PeaksWidths)
    [TraceCnt,ChCnt] = size(ChHasLen);
    AnalyzeResults = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        PeakInCh = logical(cellfun(@length,PeaksMags(TraceNo,:)));
        TraceChNames = ChNames(PeakInCh);
        TracePeaksMags = PeaksMags(TraceNo,PeakInCh);
        TracePeaksFreqs = PeaksFreqs(TraceNo,PeakInCh);
        TracePeaksWidths = PeaksWidths(TraceNo,PeakInCh);
        [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
        
        TrainMagMean = TrainData.MagMean(TrainChIdx);
        TrainMagStd = TrainData.MagStd(TrainChIdx);
        TrainFreqMean = TrainData.FreqMean(TrainChIdx);
        TrainFreqStd = TrainData.FreqStd(TrainChIdx);
        
        TracePeaksMags = TracePeaksMags(AnalyzeChIdx);
        TracePeaksFreqs = TracePeaksFreqs(AnalyzeChIdx);
        TracePeaksWidths = TracePeaksWidths(AnalyzeChIdx);
        
        ValidChCnt = length(TracePeaksMags);
        for ChNo = 1:ValidChCnt
            [~, idx] = sort(TracePeaksFreqs{ChNo});
            TracePeaksMags{ChNo} = TracePeaksMags{ChNo}(idx);
            TracePeaksFreqs{ChNo} = TracePeaksFreqs{ChNo}(idx);
            TracePeaksWidths{ChNo} = TracePeaksWidths{ChNo}(idx);
            
            [~, idx] = sort(TrainFreqMean{ChNo});
            TrainMagMean{ChNo} = TrainMagMean{ChNo}(idx);
            TrainMagStd{ChNo} = TrainMagStd{ChNo}(idx);
            TrainFreqMean{ChNo} = TrainFreqMean{ChNo}(idx);
            TrainFreqStd{ChNo} = TrainFreqStd{ChNo}(idx);
        end
        
        TotalScore = 0;
        for ChNo = 1:ValidChCnt
            PeakCnt = length(TracePeaksMags{ChNo});
            ClusterCnt = length(TrainMagMean{ChNo});
            ClustersUsed = false(ClusterCnt,1);
            TracePeaksUsed = false(PeakCnt,1);
            
            Score = 0;
            for PeakNo = 1:PeakCnt
                PeakScore = inf;
                for ClusterNo = 1:ClusterCnt
                    NewPeakScore = ...
                        abs(TracePeaksMags{ChNo}(PeakNo) - TrainMagMean{ChNo}(ClusterNo))/...
                        (TrainMagStd{ChNo}(ClusterNo)  + 0.00001) + ...
                        abs(TracePeaksFreqs{ChNo}(PeakNo) - TrainFreqMean{ChNo}(ClusterNo))/...
                        (TrainFreqStd{ChNo}(ClusterNo) + 0.00001);
                    if (NewPeakScore < PeakScore) && (~ClustersUsed(ClusterNo))
                        PeakScore = NewPeakScore;
                        ClusterUsedIdx = ClusterNo;
                    end
                end
                if ~isinf(PeakScore)
                    Score = Score + PeakScore;
                    ClustersUsed(ClusterUsedIdx) = true;
                    TracePeaksUsed(PeakNo) = true;
                end
            end
            
            TotalScore = Score/sum(double(TracePeaksUsed)) + TotalScore;
        end
        AnalyzeResults{TraceNo}.Score = TotalScore/ValidChCnt;
    end
end