function SaveResults (Results,Names,TNames,ANames,TOutput,AOutput)
    MethodCnt = length(Names);
    TrainCnt = length(TNames);
    AnalyzeCnt = length(ANames);
    TrainCell = cell(MethodCnt,TrainCnt);
    AnalyzeCell = cell(MethodCnt,AnalyzeCnt);
    for MethodNo = 1:MethodCnt
        for TraceNo = 1:TrainCnt
            TrainCell{MethodNo,TraceNo} = ...
                Results{MethodNo}.AnalyzeTrain{TraceNo}.Score;
        end
        for TraceNo = 1:AnalyzeCnt
            AnalyzeCell{MethodNo,TraceNo} = ...
                Results{MethodNo}.Analyze{TraceNo}.Score;
        end
    end
%     TrainTable = cell2table(TrainCell,'VariableNames',...
%         matlab.lang.makeValidName(TNames),'RowNames',Names);
%     AnalyzeTable = ...
%         cell2table(AnalyzeCell,'VariableNames',...
%         matlab.lang.makeValidName(ANames),'RowNames',Names);
%     writetable(TrainTable,TOutput,'FileType','text','WriteRowNames',true);
%     writetable(AnalyzeTable,AOutput,'FileType','text','WriteRowNames',true);
    TrainCell = [Names, TrainCell];
    AnalyzeCell = [Names, AnalyzeCell];
    TrainCell = [[{'Method'},TNames'];TrainCell];
    AnalyzeCell = [[{'Method'},ANames'];AnalyzeCell];
    cell2csv(TOutput,TrainCell,',');
    cell2csv(AOutput,AnalyzeCell,',');
end