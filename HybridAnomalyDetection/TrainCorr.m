function TrainData = TrainCorr(ChNames,ChHasLen,AllCorr)
    [~,ChCnt] = size(ChHasLen);
    CorrAvg = NaN(ChCnt,1);
    CorrStd = NaN(ChCnt,1);
    CorrMin = NaN(ChCnt,1);
    CorrMax = NaN(ChCnt,1);
    for ChNo = 1:ChCnt
        if sum(ChHasLen(:,ChNo)) < 2
            continue
        end
        ChCorr = AllCorr(ChHasLen(:,ChNo),ChNo);
        CorrAvg(ChNo) = mean(ChCorr);
        CorrStd(ChNo) = std(ChCorr);
        CorrMin(ChNo) = min(ChCorr);
        CorrMax(ChNo) = max(ChCorr);
    end
    ValidIdx = ~isnan(CorrMin);
    TrainData.ChNames = ChNames(ValidIdx);
    TrainData.CorrAvg = CorrAvg(ValidIdx);
    TrainData.CorrStd = CorrStd(ValidIdx);
    TrainData.CorrMin = CorrMin(ValidIdx);
    TrainData.CorrMax = CorrMax(ValidIdx);
end