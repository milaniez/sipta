function [AllFreqSeries, ChNames] = ...
            GetAllFreqSeries(SortInfoDir,MainDir,FileName)
    ChNames =  importdata([SortInfoDir '\type_name.txt']);
    ChCnt =    importdata([SortInfoDir '\type_info.txt']);
    TraceCnt = importdata([MainDir     '\trace_info.txt']);
    AllFreqSeries = cell(TraceCnt,ChCnt);
    for ChNo = 1:ChCnt
        ChNos = NaN(1,TraceCnt);
        ChNosMap = importdata([SortInfoDir '\type_list' ...
            int2str(ChNo - 1) '.txt']);
        for Entry = ChNosMap'
            ChNos(Entry(1) + 1) = Entry(2);
        end
        AllFreqSeries(:,ChNo) = GetNormFreqSerieses(ChNos,MainDir,FileName);
        ChNo
    end
end

function [FreqSerieses] = ...
    GetNormFreqSerieses(ChNos,MainDir,FileName)
    TraceCnt = length(ChNos);
    FreqSerieses = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        if isnan(ChNos(TraceNo))
            FreqSerieses{TraceNo} = nan;
            continue
        end
        FileLocation = [MainDir '\trace' int2str(TraceNo - 1)...
            '\sig' int2str(ChNos(TraceNo)) '\' FileName];
        FreqSerieses{TraceNo} = GetNormFreqSeries(FileLocation);
    end
end

function [FreqSeries] = GetNormFreqSeries(FileLocation)
    if exist(FileLocation, 'file') ~= 2
        FreqSeries = nan;
        return
    end
    Data = importdata(FileLocation);
    if isempty(Data)
        FreqSeries = nan;
        return
    end
    TimeSeries = diff(Data);
    FreqSeries = abs(fft(TimeSeries));
end