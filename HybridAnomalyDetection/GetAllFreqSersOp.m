function [AllFreqSers, AllChLens, ChNames] = ...
    GetAllFreqSersOp(SortInfoDir,MainDir,FileName,Option)
    
    if strcmp(Option, 'Norm')
        [AllFreqSeries, ChNames] = ...
            GetAllNormFreqSeries(SortInfoDir,MainDir,FileName);
    elseif strcmp(Option, 'Log')
        [AllFreqSeries, ChNames] = ...
            GetAllLogFreqSeries(SortInfoDir,MainDir,FileName);
    elseif strcmp(Option, 'LogNorm')
        [AllFreqSeries, ChNames] = ...
            GetAllLogNormFreqSeries(SortInfoDir,MainDir,FileName);
    elseif strcmp(Option, 'NormLog')
        [AllFreqSeries, ChNames] = ...
            GetAllNormLogFreqSeries(SortInfoDir,MainDir,FileName);
    elseif strcmp(Option, 'Non')
        [AllFreqSeries, ChNames] = ...
            GetAllFreqSeries(SortInfoDir,MainDir,FileName);
    else
        error('Invalid Normalization Option');
    end
    
    [TraceCnt,ChCnt] = size(AllFreqSeries);
    AllChLens = NaN(TraceCnt,ChCnt);
    for TraceNo = 1:TraceCnt
        for ChNo = 1:ChCnt
            if ~isnan(AllFreqSeries{TraceNo,ChNo})
                AllChLens(TraceNo,ChNo) = ...
                    length(AllFreqSeries{TraceNo,ChNo});
            elseif isempty(AllFreqSeries{TraceNo,ChNo})
                AllChLens(TraceNo,ChNo) = ...
                    length(AllFreqSeries{TraceNo,ChNo});
            else
                'a';
            end
            if length(AllFreqSeries{TraceNo,ChNo}) == 1
                AllFreqSeries{TraceNo,ChNo} = 1;
            end
        end
    end
    
    AllFreqSers = cell(TraceCnt,ChCnt);
    for TraceNo = 1:TraceCnt
        for ChNo = 1:ChCnt
            if and(~isnan(AllChLens(TraceNo,ChNo)),AllChLens(TraceNo,ChNo) ~= 0)
                AllFreqSers{TraceNo,ChNo}.Freq = ...
                    ((0:AllChLens(TraceNo,ChNo))/AllChLens(TraceNo,ChNo))';
                AllFreqSers{TraceNo,ChNo}.Mag = ...
                    zeros(AllChLens(TraceNo,ChNo) + 1,1);
                AllFreqSers{TraceNo,ChNo}.Mag(1:AllChLens(TraceNo,ChNo)) = ...
                    AllFreqSeries{TraceNo,ChNo};
                AllFreqSers{TraceNo,ChNo}.Mag(AllChLens(TraceNo,ChNo) + 1) = ...
                    AllFreqSeries{TraceNo,ChNo}(1);
            elseif AllChLens(TraceNo,ChNo) == 0
                AllFreqSers{TraceNo,ChNo}.Mag = zeros(0,1);
                AllFreqSers{TraceNo,ChNo}.Freq = zeros(0,1);
            elseif isnan(AllChLens(TraceNo,ChNo))
                AllFreqSers{TraceNo,ChNo}.Mag = NaN(1);
                AllFreqSers{TraceNo,ChNo}.Freq = NaN(1);
            else
                error('The error that should not be ... .');
            end
            AllFreqSers{TraceNo,ChNo}.MinDist = AllChLens(TraceNo,ChNo);
        end
    end
end