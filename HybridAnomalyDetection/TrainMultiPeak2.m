function TrainData = TrainMultiPeak2(Config,ChNames,ChHasLen,...
        PeaksMags,PeaksFreqs,PeaksWidths,PeaksRegs,PeaksFreqRegs)
    
    MaxPeakCnt = str2double(Config.PeakCnt.Text);
    MaxPresPeakCnt = str2double(Config.MaxPresPeakCntRatio.Text)*MaxPeakCnt;
    ClustMethod = Config.ClusteringMethod.Text;
    [~,ChCnt] = size(ChHasLen);
    Mag = cell(ChCnt,1);
    Freq = cell(ChCnt,1);
    Width = cell(ChCnt,1);
    Reg = cell(ChCnt,1);
    FreqReg = cell(ChCnt,1);
    MagMean = cell(ChCnt,1);
    MagStd = cell(ChCnt,1);
    FreqMean = cell(ChCnt,1);
    FreqStd = cell(ChCnt,1);
    PeakTraceMap = cell(ChCnt,1);
    ValidChs = true(ChCnt,1);
    for ChNo = 1:ChCnt
        CellsLens = cellfun(@length,PeaksFreqs(:,ChNo));
        EmptyCells = (CellsLens == 0);
        OverPresPeakCells = (CellsLens > MaxPresPeakCnt);
        InvalidCells = EmptyCells | OverPresPeakCells;
        InvalidCellCnt = sum(InvalidCells);
        ValidCellCnt = length(PeaksFreqs(:,ChNo)) - InvalidCellCnt;
        if ValidCellCnt <= 1
            ValidChs(ChNo) = false;
            continue
        end
        ChPeaksFreqs = PeaksFreqs(~InvalidCells,ChNo);
        ChPeaksMags = PeaksMags(~InvalidCells,ChNo);
        ChPeaksWidths = PeaksWidths(~InvalidCells,ChNo);
        ChPeaksRegs = PeaksRegs(~InvalidCells,ChNo);
        ChPeaksFreqRegs = PeaksFreqRegs(~InvalidCells,ChNo);
        InitClustCnt = max(cellfun(@length,PeaksFreqs(:,ChNo)));
        PeakBelongsTo = zeros(0,1);
        for CellNo = 1:ValidCellCnt
            PeakBelongsTo = [PeakBelongsTo; ...
                CellNo*ones(length(ChPeaksFreqs{CellNo}),1)]; %#ok<AGROW>
        end
        CombChPeaksFreqs = cell2mat(ChPeaksFreqs);
        CombChPeaksMags = cell2mat(ChPeaksMags);
        CombChPeaksWidths = cell2mat(ChPeaksWidths);
        CombChPeaksRegs = cell2mat(ChPeaksRegs);
        CombChPeaksFreqRegs = cell2mat(ChPeaksFreqRegs);
        if strcmpi(ClustMethod,'kmeans')
            [ClustIdx,ClustCentroids,~,~] = kmeans(CombChPeaksFreqs,InitClustCnt);
            ClustsFreqs = cell(InitClustCnt,1);
            ClustsMags = cell(InitClustCnt,1);
            ClustsWidths = cell(InitClustCnt,1);
            ClustsRegs = cell(InitClustCnt,1);
            ClustsFreqRegs = cell(InitClustCnt,1);
            ClustPeakBelongTo = cell(InitClustCnt,1);
            for ClustNo = 1:InitClustCnt
                ClustsFreqs{ClustNo} = CombChPeaksFreqs(ClustIdx == ClustNo);
                ClustsMags{ClustNo} = CombChPeaksMags(ClustIdx == ClustNo);
                ClustsWidths{ClustNo} = CombChPeaksWidths(ClustIdx == ClustNo);
                ClustsRegs{ClustNo} = CombChPeaksRegs(ClustIdx == ClustNo);
                ClustsFreqRegs{ClustNo} = CombChPeaksFreqRegs(ClustIdx == ClustNo);
                ClustPeakBelongTo{ClustNo} = PeakBelongsTo(ClustIdx == ClustNo);
            end
            % Getting rid of peaks from same trace
            UniqueClustPeakBelongTo = cellfun(@unique,ClustPeakBelongTo,'UniformOutput',false);
            UniqueClustSizes = cellfun(@length,UniqueClustPeakBelongTo);
            ClustSizes = cellfun(@length,ClustPeakBelongTo);
            for ClustNo = 1:InitClustCnt
                if UniqueClustSizes(ClustNo) == ClustSizes(ClustNo)
                    continue
                end
                RepIdx = (diff(ClustPeakBelongTo{ClustNo}) == 0);
                RepIdxPos = find(RepIdx,sum(RepIdx),'last');
                for repIdx = RepIdxPos'
                    if abs(ClustCentroids(ClustNo) - ...
                            ClustsFreqs{ClustNo}(repIdx)) > ...
                            abs(ClustCentroids(ClustNo) - ...
                            ClustsFreqs{ClustNo}(repIdx + 1))
                        ClustsFreqs{ClustNo}(repIdx) = [];
                        ClustsMags{ClustNo}(repIdx) = [];
                        ClustsWidths{ClustNo}(repIdx) = [];
                        ClustsRegs{ClustNo}(repIdx) = [];
                        ClustsFreqRegs{ClustNo}(repIdx) = [];
                        ClustPeakBelongTo{ClustNo}(repIdx) = [];
                    else
                        ClustsFreqs{ClustNo}(repIdx + 1) = [];
                        ClustsMags{ClustNo}(repIdx + 1) = [];
                        ClustsWidths{ClustNo}(repIdx + 1) = [];
                        ClustsRegs{ClustNo}(repIdx + 1) = [];
                        ClustsFreqRegs{ClustNo}(repIdx + 1) = [];
                        ClustPeakBelongTo{ClustNo}(repIdx + 1) = [];
                    end
                end
            end
        elseif strcmpi(ClustMethod,'Hierarchical')
            PeakDist = pdist(CombChPeaksFreqs);
            PeakLink = linkage(PeakDist);
            ClustCnts = InitClustCnt:length(CombChPeaksFreqs);
            ClustIdxs = cluster(PeakLink,'maxclust',ClustCnts);
            for idx = 1:length(ClustCnts)
                ClustCnt = ClustCnts(idx);
                ClustIdx = ClustIdxs(:,idx);
                ClustsFreqs = cell(ClustCnt,1);
                ClustsMags = cell(InitClustCnt,1);
                ClustsWidths = cell(InitClustCnt,1);
                ClustsRegs = cell(InitClustCnt,1);
                ClustsFreqRegs = cell(InitClustCnt,1);
                ClustPeakBelongTo = cell(ClustCnt,1);
                for ClustNo = 1:ClustCnt
                    ClustsFreqs{ClustNo} = CombChPeaksFreqs(ClustIdx == ClustNo);
                    ClustsMags{ClustNo} = CombChPeaksMags(ClustIdx == ClustNo);
                    ClustsWidths{ClustNo} = CombChPeaksWidths(ClustIdx == ClustNo);
                    ClustsRegs{ClustNo} = CombChPeaksRegs(ClustIdx == ClustNo);
                    ClustsFreqRegs{ClustNo} = CombChPeaksFreqRegs(ClustIdx == ClustNo);
                    ClustPeakBelongTo{ClustNo} = PeakBelongsTo(ClustIdx == ClustNo);
                end
                UniqueClustPeakBelongTo = cellfun(@unique,ClustPeakBelongTo,'UniformOutput',false);
                UniqueClustSizes = cellfun(@length,UniqueClustPeakBelongTo);
                ClustSizes = cellfun(@length,ClustPeakBelongTo);
                ClusterCntCorret = true;
                for ClustNo = 1:ClustCnt
                    if UniqueClustSizes(ClustNo) ~= ClustSizes(ClustNo)
                        ClusterCntCorret = false;
                        break
                    end
                end
                if ClusterCntCorret
                    break
                end
            end
        else
            error('invalid Clustering Method');
        end
        % Getting rid of clusters with less than 2 elements, as they make
        % statistical analysis impossible
        ClustLengths = cellfun(@length,ClustsFreqs);
        ClustsFreqs(ClustLengths == 1) = [];
        ClustsMags(ClustLengths == 1) = [];
        ClustsWidths(ClustLengths == 1) = [];
        ClustsRegs(ClustLengths == 1) = [];
        ClustsFreqRegs(ClustLengths == 1) = [];
        ClustPeakBelongTo(ClustLengths == 1) = [];
        if isempty(ClustsFreqs)
            ValidChs(ChNo) = false;
            continue
        end
        Freq{ChNo} = ClustsFreqs;
        Mag{ChNo} = ClustsMags;
        Width{ChNo} = ClustsWidths;
        Reg{ChNo} = ClustsRegs;
        FreqReg{ChNo} = ClustsFreqRegs;
        MagMean{ChNo} = cellfun(@mean,ClustsMags);
        MagStd{ChNo} = cellfun(@std,ClustsMags);
        FreqMean{ChNo} = cellfun(@mean,ClustsFreqs);
        FreqStd{ChNo} = cellfun(@std,ClustsFreqs);
        PeakTraceMap{ChNo} = ClustPeakBelongTo;
    end
    TrainData.ChNames = ChNames(ValidChs);
    TrainData.Freq = Freq(ValidChs);
    TrainData.Mag = Mag(ValidChs);
    TrainData.Width = Width(ValidChs);
    TrainData.Reg = Reg(ValidChs);
    TrainData.FreqReg = FreqReg(ValidChs);
    TrainData.MagMean = MagMean(ValidChs);
    TrainData.MagStd = MagStd(ValidChs);
    TrainData.FreqMean = FreqMean(ValidChs);
    TrainData.FreqStd = FreqStd(ValidChs);
    TrainData.PeakTraceMap = PeakTraceMap(ValidChs);
end