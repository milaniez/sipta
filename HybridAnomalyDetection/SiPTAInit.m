function Results = SiPTAInit (ConfigFile)
%%  Initialize
%     Read Config File
    Config = xml2struct(ConfigFile);
    CleanTraceDir = Config.All.PreProc.DataSettings.CleanTracesDir.Text;
    DirtyTraceDir = Config.All.PreProc.DataSettings.DirtyTracesDir.Text;
    CleanWildcard = Config.All.PreProc.DataSettings.CleanWildcard.Text;
    DirtyWildcard = Config.All.PreProc.DataSettings.DirtyWildcard.Text;
    TimeSersSrc = Config.All.PreProc.SpecSettings.TimeSersSrc.Text;
    TimeSersConst = Config.All.PreProc.SpecSettings.TimeSersConst.Text;
    ChColsNames = strsplit(Config.All.PreProc.SpecSettings.ChCols.Text,'-');
    SpecOption = Config.All.PreProc.SpecSettings.SpecOption.Text;
    CleanTraceList = dir(fullfile(CleanTraceDir,CleanWildcard));
    CleanTraceList([CleanTraceList(:).isdir] == true) = [];
    DirtyTraceList = dir(fullfile(DirtyTraceDir,DirtyWildcard));
    DirtyTraceList([DirtyTraceList(:).isdir] == true) = [];
    CleanTraceCnt = length(CleanTraceList);
    DirtyTraceCnt = length(DirtyTraceList);
    AllCleanSers = cell(CleanTraceCnt,1);
    parfor CleanTraceNo = 1:CleanTraceCnt
        CleanTraceNo
        TraceTable = readtable(fullfile(CleanTraceDir,...
            CleanTraceList(CleanTraceNo).name),'Format','%u64%s%s%s%s');
        ColNames = TraceTable.Properties.VariableNames;
        [~,ColNos,~] = intersect(ColNames,ChColsNames);
        ColNos = sort(ColNos);
        TraceTable = table2cell(TraceTable);
        TimeCol = cell2mat(TraceTable(:,1));
        StartTime = double(TimeCol(1));
        EndTime = double(TimeCol(length(TimeCol)));
        ChCols = TraceTable(:,ColNos);
        [EntryCnt,~] = size(ChCols);
        ChCol = cell(EntryCnt,1);
        for EntryNo = 1:EntryCnt
            ChCol{EntryNo} = strjoin(ChCols(EntryNo,:),'::');
        end
        [ChNames,~,RowIdx] = unique(ChCol);
        ChCnt = length(ChNames);
        TraceTimeSers = cell(ChCnt,1);
        TraceFreqSers = cell(ChCnt,1);
        for ChNo = 1:ChCnt
            TimeStmps = TimeCol(RowIdx == ChNo);
            'here'
            if strcmpi(TimeSersConst,'Diff')
                TimeSer = diff(TimeStmps);
            elseif strcmpi(TimeSersConst,'WinTS')
                WinOp = Config.All.PreProc.SpecSettings.WindowOption.Text;
                TimeStmpsLen = length(TimeStmps);
                if strcmpi(WinOp,'WindowLength');
                    WinLen = str2double(Config.All.PreProc.SpecSettings.WindowLen.Text);
                    WinDiff = str2double(Config.All.PreProc.SpecSettings.WindowDiff.Text);
                    WinCnt = 1 + floor((EndTime - StartTime)/WinDiff);
                elseif strcmpi(WinOp,'WindowCount');
                    WinCnt = str2double(Config.All.PreProc.SpecSettings.WindowCount.Text);
                    WinDiffRat = str2double(Config.All.PreProc.SpecSettings.WindowDiffRatio.Text);
                    WinDiff = (EndTime - StartTime + 1)/(WinCnt - 1);
                    WinLen = WinDiff/WinDiffRat;
                else
                    error('invalid option');
                end
                TimeSer = zeros(WinCnt,1);
                for TimeStmpIdx = 1:TimeStmpsLen
                    TimeStmp = TimeStmps(TimeStmpIdx) - StartTime;
                    for TimeSerIdx = ...
                            max(1,ceil((TimeStmp - WinLen)/WinDiff)):...
                            min(WinCnt,ceil((TimeStmp - WinLen)/WinDiff))
                        TimeSer(TimeSerIdx) = TimeSer(TimeSerIdx) + 1;
                    end
                end
            else
                error('Invalid Time Series Construction Method');
            end
            TraceTimeSers{ChNo} = double(TimeSer);
            FreqSer = fft(TraceTimeSers{ChNo});
            FreqSerLen = length(FreqSer);
            if FreqSerLen > 1
                if strcmpi(SpecOption,'Norm')
                    FreqSer = FreqSer*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)));
                elseif strcmpi(SpecOption,'NormAbs')
                    FreqSer = abs(FreqSer)*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)));
                elseif strcmpi(SpecOption,'NormPer')
                    FreqSer = abs(FreqSer).^2*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)).^2);
                else
                    error('Invalid method');
                end
            elseif FreqSerLen ==  1
            	FreqSer = 1;
            end
            if ~isempty(FreqSer)
                FreqSer(1) = FreqSer(1)/2;
                TraceFreqSers{ChNo}.Mag = [FreqSer; FreqSer(1)];
                TraceFreqSers{ChNo}.Freq = (0:1/FreqSerLen:1)';
                TraceFreqSers{ChNo}.MinDist = FreqSerLen;
            else
                TraceFreqSers{ChNo}.Mag = []';
                TraceFreqSers{ChNo}.Freq = []';
                TraceFreqSers{ChNo}.MinDist = 0;
            end
        end
        AllCleanSers{CleanTraceNo}.TimeSers = TraceTimeSers;
        AllCleanSers{CleanTraceNo}.FreqSers = TraceFreqSers;
        AllCleanSers{CleanTraceNo}.ChNames = ChNames;
    end
    AllDirtySers = cell(DirtyTraceCnt,1);
    parfor DirtyTraceNo = 1:DirtyTraceCnt
        DirtyTraceNo
        TraceTable = readtable(fullfile(DirtyTraceDir,...
            DirtyTraceList(DirtyTraceNo).name),'Format','%u64%s%s%s%s');
        ColNames = TraceTable.Properties.VariableNames;
        [~,ColNos,~] = intersect(ColNames,ChColsNames);
        ColNos = sort(ColNos);
        TraceTable = table2cell(TraceTable);
        TimeCol = cell2mat(TraceTable(:,1));
        ChCols = TraceTable(:,ColNos);
        StartTime = double(TimeCol(1));
        EndTime = double(TimeCol(length(TimeCol)));
        [EntryCnt,~] = size(ChCols);
        ChCol = cell(EntryCnt,1);
        for EntryNo = 1:EntryCnt
            ChCol{EntryNo} = strjoin(ChCols(EntryNo,:),'::');
        end
        [ChNames,~,RowIdx] = unique(ChCol);
        ChCnt = length(ChNames);
        TraceTimeSers = cell(ChCnt,1);
        TraceFreqSers = cell(ChCnt,1);
        for ChNo = 1:ChCnt
            TimeStmps = TimeCol(RowIdx == ChNo);
            'here'
            if strcmpi(TimeSersConst,'Diff')
                TimeSer = diff(TimeStmps);
            elseif strcmpi(TimeSersConst,'WinTS')
                WinOp = Config.All.PreProc.SpecSettings.WindowOption.Text;
                TimeStmpsLen = length(TimeStmps);
                if strcmpi(WinOp,'WindowLength');
                    WinLen = str2double(Config.All.PreProc.SpecSettings.WindowLen.Text);
                    WinDiff = str2double(Config.All.PreProc.SpecSettings.WindowDiff.Text);
                    WinCnt = 1 + floor((EndTime - StartTime)/WinDiff);
                elseif strcmpi(WinOp,'WindowCount');
                    WinCnt = str2double(Config.All.PreProc.SpecSettings.WindowCount.Text);
                    WinDiffRat = str2double(Config.All.PreProc.SpecSettings.WindowDiffRatio.Text);
                    WinDiff = (EndTime - StartTime + 1)/(WinCnt - 1);
                    WinLen = WinDiff/WinDiffRat;
                else
                    error('invalid option');
                end
                TimeSer = zeros(WinCnt,1);
                for TimeStmpIdx = 1:TimeStmpsLen
                    TimeStmp = TimeStmps(TimeStmpIdx) - StartTime;
                    for TimeSerIdx = ...
                            max(1,ceil((TimeStmp - WinLen)/WinDiff)):...
                            min(WinCnt,ceil((TimeStmp - WinLen)/WinDiff))
                        TimeSer(TimeSerIdx) = TimeSer(TimeSerIdx) + 1;
                    end
                end
            else
                error('Invalid Time Series Construction Method');
            end
            TraceTimeSers{ChNo} = double(TimeSer);
            FreqSer = abs(fft(TraceTimeSers{ChNo}));
            FreqSerLen = length(FreqSer);
            if FreqSerLen > 1
                if strcmpi(SpecOption,'Norm')
                    FreqSer = FreqSer*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)));
                elseif strcmpi(SpecOption,'NormAbs')
                    FreqSer = abs(FreqSer)*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)));
                elseif strcmpi(SpecOption,'NormPer')
                    FreqSer = abs(FreqSer).^2*...
                        (FreqSerLen - 1)/...
                        sum(abs(FreqSer(2:FreqSerLen)).^2);
                else
                    error('Invalid method');
                end
            elseif FreqSerLen ==  1
            	FreqSer = 1;
            end
            if ~isempty(FreqSer)
                FreqSer(1) = FreqSer(1)/2;
                TraceFreqSers{ChNo}.Mag = [FreqSer; FreqSer(1)];
                TraceFreqSers{ChNo}.Freq = (0:1/FreqSerLen:1)';
                TraceFreqSers{ChNo}.MinDist = FreqSerLen;
            else
                TraceFreqSers{ChNo}.Mag = []';
                TraceFreqSers{ChNo}.Freq = []';
                TraceFreqSers{ChNo}.MinDist = 0;
            end
        end
        AllDirtySers{DirtyTraceNo}.TimeSers = TraceTimeSers;
        AllDirtySers{DirtyTraceNo}.FreqSers = TraceFreqSers;
        AllDirtySers{DirtyTraceNo}.ChNames = ChNames;
    end
%%  Return Results
    Results.CleanTraceNames = {CleanTraceList.name}';
    Results.DirtyTraceNames = {DirtyTraceList.name}';
    Results.AllCleanSers = AllCleanSers;
    Results.AllDirtySers = AllDirtySers;
end