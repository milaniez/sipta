function [AllLogNormFreqSeries, ChNames] = ...
            GetAllLogNormFreqSeries(SortInfoDir,MainDir,FileName)
    ChNames =  importdata([SortInfoDir '\type_name.txt']);
    ChCnt =    importdata([SortInfoDir '\type_info.txt']);
    TraceCnt = importdata([MainDir     '\trace_info.txt']);
    AllLogNormFreqSeries = cell(TraceCnt,ChCnt);
    for ChNo = 1:ChCnt
        ChNos = NaN(1,TraceCnt);
        ChNosMap = importdata([SortInfoDir '\type_list' ...
            int2str(ChNo - 1) '.txt']);
        for Entry = ChNosMap'
            ChNos(Entry(1) + 1) = Entry(2);
        end
        AllLogNormFreqSeries(:,ChNo) = GetNormFreqSerieses(ChNos,MainDir,FileName);
        ChNo
    end
end

function [LogNormFreqSerieses] = ...
    GetNormFreqSerieses(ChNos,MainDir,FileName)
    TraceCnt = length(ChNos);
    LogNormFreqSerieses = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        if isnan(ChNos(TraceNo))
            LogNormFreqSerieses{TraceNo} = nan;
            continue
        end
        FileLocation = [MainDir '\trace' int2str(TraceNo - 1)...
            '\sig' int2str(ChNos(TraceNo)) '\' FileName];
        LogNormFreqSerieses{TraceNo} = GetNormFreqSeries(FileLocation);
    end
end

function [LogNormFreqSeries] = GetNormFreqSeries(FileLocation)
    if exist(FileLocation, 'file') ~= 2
        LogNormFreqSeries = nan;
        return
    end
    Data = importdata(FileLocation);
    if isempty(Data)
        LogNormFreqSeries = nan;
        return
    end
    TimeSeries = diff(Data);
    FreqSeries = mag2db(abs(fft(TimeSeries)));
    FreqSeriesLen = length(FreqSeries);
    if FreqSeriesLen == 0
        LogNormFreqSeries = [];
        return
    end
    LogNormFreqSeries = mag2db(FreqSeries/...
        (sum(FreqSeries)/FreqSeriesLen));
end