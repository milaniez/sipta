function [AllNormFreqSeries, ChNames] = ...
            GetAllNormFreqSeries(SortInfoDir,MainDir,FileName)
    ChNames =  importdata([SortInfoDir '\type_name.txt']);
    ChCnt =    importdata([SortInfoDir '\type_info.txt']);
    TraceCnt = importdata([MainDir     '\trace_info.txt']);
    AllNormFreqSeries = cell(TraceCnt,ChCnt);
    for ChNo = 1:ChCnt
        ChNos = NaN(1,TraceCnt);
        ChNosMap = importdata([SortInfoDir '\type_list' ...
            int2str(ChNo - 1) '.txt']);
        for Entry = ChNosMap'
            ChNos(Entry(1) + 1) = Entry(2);
        end
        AllNormFreqSeries(:,ChNo) = GetNormFreqSerieses(ChNos,MainDir,FileName);
        ChNo
    end
end

function [NormFreqSerieses] = ...
    GetNormFreqSerieses(ChNos,MainDir,FileName)
    TraceCnt = length(ChNos);
    NormFreqSerieses = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        if isnan(ChNos(TraceNo))
            NormFreqSerieses{TraceNo} = nan;
            continue
        end
        FileLocation = [MainDir '\trace' int2str(TraceNo - 1)...
            '\sig' int2str(ChNos(TraceNo)) '\' FileName];
        NormFreqSerieses{TraceNo} = GetNormFreqSeries(FileLocation);
    end
end

function [NormFreqSeries] = GetNormFreqSeries(FileLocation)
    if exist(FileLocation, 'file') ~= 2
        NormFreqSeries = nan;
        return
    end
    Data = importdata(FileLocation);
    if isempty(Data)
        NormFreqSeries = nan;
        return
    end
    TimeSeries = diff(Data);
    FreqSeries = abs(fft(TimeSeries));
    FreqSeriesLen = length(FreqSeries);
    if FreqSeriesLen == 0
        NormFreqSeries = [];
        return
    end
    if FreqSeriesLen == 1
        NormFreqSeries = FreqSeries;
        return
    end
    NormFreqSeries = FreqSeries/(sum(FreqSeries)/FreqSeriesLen);
end