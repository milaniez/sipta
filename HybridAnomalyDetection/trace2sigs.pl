use strict;
use warnings;

# ARGV[0]: input file, ARGV[1]: output directory, ARGV[2 ... n + 1]: fields representing signals. other fields rpresent channels for signals

# check whether the input argumets are enough
if ($#ARGV < 2)
{
	die "not enough input arguments";
}

my %argvHash = (); # Hash to ARGV values
for (my $i = 2; $i <= $#ARGV; $i++)
{
	$argvHash{"$ARGV[$i]"} = 1;
}

# if ((exists $argvHash{"0"}) || (exists $argvHash{"1"}))
# {
	# die "none allowed columns 0 or 1 were used as field signals";
# }

open (TRACE, $ARGV[0]) or die "cannot open input file"; # open trace file
open (SIGLIST, ">$ARGV[1]"."\/"."sig_list.txt") or die "cannot open sig_list output file"; # create signal list file
open (SIGINFO, ">$ARGV[1]"."\/"."sig_info.txt") or die "cannot open sig_info output file"; # create signal info file

my %SigListHash = (); # Hash table for mapping signals into corresponding folder
my $SigCnt = 0; # Signal Count
my @SigFileList = ();
my @entryFileList = ();
my @tFileList = ();
my @SigInfoFileList = ();
my @SigLenList = ();

my $Line = 0;
my @SplittedLine = ();
my $LineCnt = 0;

my $OutputLine = 0;
my $OutputLineEmpty = 0;

my $entry = 0;
# Write data to channel and list files
while ($Line = <TRACE>)
{
	if (!($Line =~ /^time,class,event,pid,pname$/))
	{
		$entry = $entry + 1;
		# if (($LineCnt % 100) == 0)
		# {
			# print "$LineCnt\n";
		# }
		$LineCnt++;
		@SplittedLine = split(/,/,$Line);
		my $SigListHashKey = "$SplittedLine[$ARGV[2]]";
		for (my $i = 3; $i <= $#ARGV; $i++)
		{
			$SigListHashKey = "$SigListHashKey"." $SplittedLine[$ARGV[$i]]";
		}
		if (! exists $SigListHash{$SigListHashKey})
		{
			$SigListHash{$SigListHashKey} = $SigCnt;
			my $SigListFileLine = "$SigCnt: ";
			for (my $i = 2; $i <= $#ARGV; $i++)
			{
				$SigListFileLine = "$SigListFileLine"." $ARGV[$i]:$SplittedLine[$ARGV[$i]]";
			}
			SIGLIST -> print ("$SigListFileLine\n");
			mkdir ("$ARGV[1]\/sig$SigCnt");
			open (my $SigFile,     ">$ARGV[1]\/sig$SigCnt\/sig.txt");
			push (@SigFileList, $SigFile);
			open (my $entryFile,   ">$ARGV[1]\/sig$SigCnt\/entry.txt");
			push (@entryFileList, $entryFile);
			open (my $rtFile,       ">$ARGV[1]\/sig$SigCnt\/t.txt");
			push (@tFileList, $rtFile);
			open (my $SigInfoFile, ">$ARGV[1]\/sig$SigCnt\/siginfo.txt");
			push (@SigInfoFileList, $SigInfoFile);
			push (@SigLenList, 0);
			$SigCnt++;
		}
		my $SigNo = $SigListHash{$SigListHashKey};
		$OutputLineEmpty = 1;
		for (my $Col = 2; $Col <= $#SplittedLine; $Col++)
		{
			if (! exists $argvHash{"$Col"})
			{
				if ($OutputLineEmpty)
				{
					$OutputLineEmpty = 0;
					$OutputLine = "$SplittedLine[$Col]";
				}
				else
				{
					$OutputLine = "$OutputLine,$SplittedLine[$Col]";
				}
			}
		}
		if (exists $argvHash{"$#SplittedLine"})
		{
			$OutputLine = "$OutputLine\n";
		}
		$SigLenList[$SigNo]++;
		$SigFileList[$SigNo] -> print ("$OutputLine");
		$SplittedLine[0] =~ /^"(.*?)"$/;
		$entryFileList[$SigNo] -> print ("$entry\n");
		$tFileList[$SigNo] -> print ("$SplittedLine[0]\n");
	}
}

SIGINFO -> print ("$SigCnt");

close (SIGINFO);
close (TRACE);
close (SIGLIST);

for (my $SigNo = 0; $SigNo < $SigCnt; $SigNo++)
{
	close ($SigFileList[$SigNo]);
	close ($entryFileList[$SigNo]);
	close ($tFileList[$SigNo]);
	$SigInfoFileList[$SigNo] -> print ("$SigLenList[$SigNo]");
	close ($SigInfoFileList[$SigNo]);
}