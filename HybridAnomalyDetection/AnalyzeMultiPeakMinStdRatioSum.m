function AnalyzeResults = AnalyzeMultiPeakMinStdRatioSum...
        (Config,TrainData,ChNames,ChHasLen,...
        PeaksMags,PeaksFreqs,PeaksWidths)
    [TraceCnt,ChCnt] = size(ChHasLen);
    AnalyzeResults = cell(TraceCnt,1);
    ScoreThreshold = str2doulbe(Config.ScoreThreshold.Text);
    for TraceNo = 1:TraceCnt
        TraceChNames = ChNames(ChHasLen(TraceNo,:));
        TracePeaksMags = PeaksMags(TraceNo,ChHasLen(TraceNo,:));
        TracePeaksFreqs = PeaksFreqs(TraceNo,ChHasLen(TraceNo,:));
        TracePeaksWidths = PeaksWidths(TraceNo,ChHasLen(TraceNo,:));
        [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
        
        TrainMagMean = TrainData.MagMean(TrainChIdx);
        TrainMagStd = TrainData.MagStd(TrainChIdx);
        TrainFreqMean = TrainData.FreqMean(TrainChIdx);
        TrainFreqStd = TrainData.FreqStd(TrainChIdx);
        
        TracePeaksMags = TracePeaksMags(AnalyzeChIdx);
        TracePeaksFreqs = TracePeaksFreqs(AnalyzeChIdx);
        TracePeaksWidths = TracePeaksWidths(AnalyzeChIdx);
        
        ValidChCnt = length(TracePeaksMags);
        for ChNo = 1:ValidChCnt
            ClusterCnt = length(TrainMagMean{ChNo});
            if length(TracePeaksMags{ChNo}) > ClusterCnt
                [~, idx] = sort(TracePeaksMags{ChNo},'descend');
                idx = idx(1:ClusterCnt);
                TracePeaksMags{ChNo} = TracePeaksMags{ChNo}(idx);
                TracePeaksFreqs{ChNo} = TracePeaksFreqs{ChNo}(idx);
                TracePeaksWidths{ChNo} = TracePeaksWidths{ChNo}(idx);
            end
            
            [~, idx] = sort(TracePeaksFreqs{ChNo});
            TracePeaksMags{ChNo} = TracePeaksMags{ChNo}(idx);
            TracePeaksFreqs{ChNo} = TracePeaksFreqs{ChNo}(idx);
            TracePeaksWidths{ChNo} = TracePeaksWidths{ChNo}(idx);
            
            [~, idx] = sort(TrainFreqMean{ChNo});
            TrainMagMean{ChNo} = TrainMagMean{ChNo}(idx);
            TrainMagStd{ChNo} = TrainMagStd{ChNo}(idx);
            TrainFreqMean{ChNo} = TrainFreqMean{ChNo}(idx);
            TrainFreqStd{ChNo} = TrainFreqStd{ChNo}(idx);
        end
        TotalScore = 0;
        TotalPeakCnt = 0;
        for ChNo = 1:ValidChCnt
            PeakCnt = length(TracePeaksMags{ChNo});
            ClusterCnt = length(TrainMagMean{ChNo});
            Score = 0;
            for PeakNo = 1:PeakCnt
                
                PeakScore = ...
                    abs(TracePeaksMags{ChNo}(PeakNo) - TrainMagMean{ChNo}(1))/...
                    (TrainMagStd{ChNo}(1) + 0.00001) + ...
                    abs(TracePeaksFreqs{ChNo}(PeakNo) - TrainFreqMean{ChNo}(1))/...
                    (TrainFreqStd{ChNo}(1) + 0.00001);
                for ClusterNo = 2:ClusterCnt
                    NewPeakScore = ...
                        abs(TracePeaksMags{ChNo}(PeakNo) - TrainMagMean{ChNo}(ClusterNo))/...
                        (TrainMagStd{ChNo}(ClusterNo)  + 0.00001) + ...
                        abs(TracePeaksFreqs{ChNo}(PeakNo) - TrainFreqMean{ChNo}(ClusterNo))/...
                        (TrainFreqStd{ChNo}(ClusterNo) + 0.00001);
                    if NewPeakScore < PeakScore
                        PeakScore = NewPeakScore;
                    end
                end
                Score = Score + PeakScore;
            end
            TotalScore = Score + TotalScore;
        end
        AnalyzeResults{TraceNo}.Score = TotalScore;
    end
end