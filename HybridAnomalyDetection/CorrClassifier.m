function Results = CorrClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf)
    
    [TraceCntT,ChCntT] = size(AllFreqSersT);
    [TraceCntA,ChCntA] = size(AllFreqSersA);
    MinChLen = str2double(Conf.MinChLen.Text);
    ChHasLenT = false(TraceCntT,ChCntT);
    ChHasLenA = false(TraceCntA,ChCntA);
    if MinChLen > 4
        for TraceNo = 1:TraceCntT
            for ChNo = 1:ChCntT
                if length(AllFreqSersT{TraceNo,ChNo}.Mag) >= MinChLen
                    ChHasLenT(TraceNo,ChNo) = true;
                end
            end
        end
        for TraceNo = 1:TraceCntA
            for ChNo = 1:ChCntA
                if length(AllFreqSersA{TraceNo,ChNo}.Mag) >= MinChLen
                    ChHasLenA(TraceNo,ChNo) = true;
                end
            end
        end
    else
        error ('Invalid Minimum Channel Length');
    end
%%  Prepare data for TrainCorr
    AllFreqSersHT = cell(TraceCntT,ChCntT);
    AllCorrT = NaN(TraceCntT,ChCntT);
    
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ChHasLenT(TraceNo,ChNo) == 1
                AllFreqSersHT{TraceNo,ChNo}.MinDist = ...
                    AllFreqSersT{TraceNo,ChNo}.MinDist;
                RedIdx = (AllFreqSersT{TraceNo,ChNo}.Freq > 0.5);
                AllFreqSersHT{TraceNo,ChNo}.Freq = ...
                    AllFreqSersT{TraceNo,ChNo}.Freq;
                AllFreqSersHT{TraceNo,ChNo}.Mag = ...
                    AllFreqSersT{TraceNo,ChNo}.Mag;
                AllFreqSersHT{TraceNo,ChNo}.Freq(RedIdx) = [];
                AllFreqSersHT{TraceNo,ChNo}.Mag(RedIdx) = [];
                AllFreqSersHT{TraceNo,ChNo}.Mag(1) = [];
                AllCorrT(TraceNo,ChNo) = ...
                    mean(abs(diff(AllFreqSersT{TraceNo,ChNo}.Mag)));
            end
        end
    end
%     Train
    Results.Train = TrainCorr(ChNamesT,ChHasLenT,AllCorrT);
%%  Prepare data for AnalyzeCorr
    AllFreqSersHA = cell(TraceCntA,ChCntA);
    AllCorrA = NaN(TraceCntA,ChCntA);
    
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ChHasLenA(TraceNo,ChNo) == 1
                AllFreqSersHA{TraceNo,ChNo}.MinDist = ...
                    AllFreqSersA{TraceNo,ChNo}.MinDist;
                RedIdx = (AllFreqSersA{TraceNo,ChNo}.Freq > 0.5);
                AllFreqSersHA{TraceNo,ChNo}.Freq = ...
                    AllFreqSersA{TraceNo,ChNo}.Freq;
                AllFreqSersHA{TraceNo,ChNo}.Mag = ...
                    AllFreqSersA{TraceNo,ChNo}.Mag;
                AllFreqSersHA{TraceNo,ChNo}.Freq(RedIdx) = [];
                AllFreqSersHA{TraceNo,ChNo}.Mag(RedIdx) = [];
                AllFreqSersHA{TraceNo,ChNo}.Mag(1) = [];
                AllCorrA(TraceNo,ChNo) = ...
                    mean(abs(diff(AllFreqSersA{TraceNo,ChNo}.Mag)));
            end
        end
    end
%     Analyze
    Type = Conf.Analyze.Type.Text;
    Results.Analyze = ...
        AnalyzeCorr(Type,Results.Train,ChNamesA,ChHasLenA,AllCorrA);
end