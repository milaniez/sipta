function TrainData = TrainDC(ChNames, ChExists, ChDCs)
    [TraceCnt,ChCnt] = size(ChExists);
    TrainData.TraceCnt = TraceCnt;
    TrainData.OrigChCnt = ChCnt;
    TrainData.OrigChNames = ChNames;
    ValidDCSigData = false(1,ChCnt);
    TrainData.DCAvg = zeros(1,ChCnt);
    TrainData.DCStd = zeros(1,ChCnt);
    TrainData.DCMin = zeros(1,ChCnt);
    TrainData.DCMax = zeros(1,ChCnt);
    TrainData.Count = zeros(1,ChCnt);
    TrainData.DCs = cell(1,ChCnt);
    for ChNo = 1:ChCnt
        if sum(ChExists(:,ChNo)) > 1
            ValidDCSigData(ChNo) = true;
            ChDC = ChDCs(ChExists(:,ChNo),ChNo);
            TrainData.DCs{ChNo} = ChDC;
            TrainData.Count(ChNo) = length(ChDC);
            TrainData.DCAvg(ChNo) = mean(ChDC);
            TrainData.DCStd(ChNo) = std(ChDC);
            TrainData.DCMin(ChNo) = min(ChDC);
            TrainData.DCMax(ChNo) = max(ChDC);
        end
    end
    TrainData.ChNames = ChNames(ValidDCSigData);
    TrainData.ChCnt = length(TrainData.ChNames);
    TrainData.DCAvg = TrainData.DCAvg(ValidDCSigData);
    TrainData.DCStd = TrainData.DCStd(ValidDCSigData);
    TrainData.DCMin = TrainData.DCMin(ValidDCSigData);
    TrainData.DCMax = TrainData.DCMax(ValidDCSigData);
    TrainData.Count = TrainData.Count(ValidDCSigData);
    TrainData.DCs = TrainData.DCs(ValidDCSigData);
end