function [AllNormLogFreqSeries, ChNames] = ...
            GetAllNormLogFreqSeries(SortInfoDir,MainDir,FileName)
    ChNames =  importdata([SortInfoDir '\type_name.txt']);
    ChCnt =    importdata([SortInfoDir '\type_info.txt']);
    TraceCnt = importdata([MainDir     '\trace_info.txt']);
    AllNormLogFreqSeries = cell(TraceCnt,ChCnt);
    for ChNo = 1:ChCnt
        ChNos = NaN(1,TraceCnt);
        ChNosMap = importdata([SortInfoDir '\type_list' ...
            int2str(ChNo - 1) '.txt']);
        for Entry = ChNosMap'
            ChNos(Entry(1) + 1) = Entry(2);
        end
        AllNormLogFreqSeries(:,ChNo) = GetNormFreqSerieses(ChNos,MainDir,FileName);
        ChNo
    end
end

function [NormLogFreqSerieses] = ...
    GetNormFreqSerieses(ChNos,MainDir,FileName)
    TraceCnt = length(ChNos);
    NormLogFreqSerieses = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        if isnan(ChNos(TraceNo))
            NormLogFreqSerieses{TraceNo} = nan;
            continue
        end
        FileLocation = [MainDir '\trace' int2str(TraceNo - 1)...
            '\sig' int2str(ChNos(TraceNo)) '\' FileName];
        NormLogFreqSerieses{TraceNo} = GetNormFreqSeries(FileLocation);
    end
end

function [NormLogFreqSeries] = GetNormFreqSeries(FileLocation)
    if exist(FileLocation, 'file') ~= 2
        NormLogFreqSeries = nan;
        return
    end
    Data = importdata(FileLocation);
    if isempty(Data)
        NormLogFreqSeries = nan;
        return
    end
    TimeSeries = diff(Data);
    LogFreqSeries = mag2db(abs(fft(TimeSeries)));
    LogFreqSeriesLen = length(LogFreqSeries);
    if LogFreqSeriesLen == 0
        NormLogFreqSeries = [];
        return
    end
    LogFreqSeriesSize = size(LogFreqSeries);
    NormLogFreqSeries = LogFreqSeries - mean(LogFreqSeries)*...
        ones(LogFreqSeriesSize(1),LogFreqSeriesSize(2));
end
























