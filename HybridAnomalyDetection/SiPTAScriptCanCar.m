clear;
Config = xml2struct('ConfigCanCarTemplate.xml');
CleanNamePattern = { ...
    '^M10_.*';
    '^M1_.*';
    '^M2_.*';
    '^M3_.*';
    '^M4_.*';
    '^M5_.*';
    '^M6_.*';
    '^M7_.*';
    '^M8_.*';
    '^M9_.*'};
Inputs = cell(6,1);
Results = cell(6,1);
Names = cell(6,1);
TNames = cell(6,1);
ANames = cell(6,1);
for i = 1:6
    
    struct2xml(Config,'Config.xml');
    Inputs{i} = SiPTAInit('Config.xml');
    [Results{i}, Names{i}, TNames{i}, ANames{i}] = SiPTAMain (Inputs{i},'Config.xml');
end