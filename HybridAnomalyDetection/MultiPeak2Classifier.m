function Results = MultiPeak2Classifier...
        (ChNamesT,AllTimeSersT,AllFreqSersT,...
         ChNamesA,AllTimeSersA,AllFreqSersA,Conf)
%%  Initialize
    [TraceCntT,ChCntT] = size(AllTimeSersT);
    [TraceCntA,ChCntA] = size(AllTimeSersA);
    MinChLen = str2double(Conf.Config.MinChLen.Text);
    ChHasLenT = zeros(TraceCntT,ChCntT);
    ChHasLenA = zeros(TraceCntA,ChCntA);
    if MinChLen > 2
        for TraceNo = 1:TraceCntT
            for ChNo = 1:ChCntT
                if length(AllTimeSersT{TraceNo,ChNo}) >= MinChLen
                    ChHasLenT(TraceNo,ChNo) = 1;
                end
            end
        end
        for TraceNo = 1:TraceCntA
            for ChNo = 1:ChCntA
                if length(AllTimeSersA{TraceNo,ChNo}) >= MinChLen
                    ChHasLenA(TraceNo,ChNo) = 1;
                end
            end
        end
    else
        error ('Invalid Minimum Channel Length');
    end
    PermCnt = str2double(Conf.Config.PermuteCnt.Text);
    PermSeed = Conf.Config.PermuteSeed.Text;
    ConfIntv = str2double(Conf.Config.ConfidenceIntv.Text);
    if ~strcmp(PermSeed,'shuffle')
        PermSeed = str2double(PermSeed);
    end
    RndStr = RandStream('mt19937ar','Seed',PermSeed);
    %   Get the values of thresholds for training traces
    AllPermPeaksT = zeros(TraceCntT,ChCntT,PermCnt);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ChHasLenT(TraceNo,ChNo) == 1
                for PermNo = 1:PermCnt
                    PermutedCh = AllTimeSersT{TraceNo,ChNo}(randperm(RndStr,...
                        length(AllTimeSersT{TraceNo,ChNo})));
                    ChPer = fft(PermutedCh);
                    ChPer = abs(ChPer);
                    ChPer = ChPer.^2;
                    ChPer(1) = [];
                    ChPer = ChPer/(sum(ChPer)/length(ChPer));
                    [PerMaxMag] = max(ChPer);
                    AllPermPeaksT(TraceNo, ChNo, PermNo) = PerMaxMag;
                end
                AllPermPeaksT(TraceNo, ChNo, :) = ...
                    sort(AllPermPeaksT(TraceNo, ChNo, :));
            end
        end
    end
    ThreshPermIdx = floor(PermCnt*ConfIntv);
    AllMinPeaksT = AllPermPeaksT(:,:,ThreshPermIdx);
    %   Get the values of thresholds for analyzing traces
    AllPermPeaksA = zeros(TraceCntA,ChCntA,PermCnt);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ChHasLenA(TraceNo,ChNo) == 1
                for PermNo = 1:PermCnt
                    PermutedCh = AllTimeSersA{TraceNo,ChNo}(randperm(RndStr,...
                        length(AllTimeSersA{TraceNo,ChNo})));
                    ChPer = fft(PermutedCh);
                    ChPer = abs(ChPer);
                    ChPer = ChPer.^2;
                    ChPer(1) = [];
                    ChPer = ChPer/(sum(ChPer)/length(ChPer));
                    [PerMaxMag] = max(ChPer);
                    AllPermPeaksA(TraceNo, ChNo, PermNo) = PerMaxMag;
                end
                AllPermPeaksA(TraceNo, ChNo, :) = ...
                    sort(AllPermPeaksA(TraceNo, ChNo, :));
            end
        end
    end
    ThreshPermIdx = floor(PermCnt*ConfIntv);
    AllMinPeaksA = AllPermPeaksA(:,:,ThreshPermIdx);
%%  Prepare data for TrainMultiPeak
    AllFreqSersHT = cell(TraceCntT,ChCntT);
    PeaksMagsHT = cell(TraceCntT,ChCntT);
    PeaksFreqsHT = cell(TraceCntT,ChCntT);
    PeaksWidthsHT = cell(TraceCntT,ChCntT);
    PeaksRegsHT = cell(TraceCntT,ChCntT);
    PeaksFreqRegsHT = cell(TraceCntT,ChCntT);
    
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ChHasLenT(TraceNo,ChNo) == 1
                AllFreqSersHT{TraceNo,ChNo}.MinDist = ...
                    AllFreqSersT{TraceNo,ChNo}.MinDist;
%                     This is not the most efficient way to do this
                RedIdx = (AllFreqSersT{TraceNo,ChNo}.Freq > 0.5);
                AllFreqSersHT{TraceNo,ChNo}.Freq = ...
                    AllFreqSersT{TraceNo,ChNo}.Freq;
                AllFreqSersHT{TraceNo,ChNo}.Mag = ...
                    AllFreqSersT{TraceNo,ChNo}.Mag;
                AllFreqSersHT{TraceNo,ChNo}.Freq(RedIdx) = [];
                AllFreqSersHT{TraceNo,ChNo}.Mag(RedIdx) = [];
                AllFreqSersHT{TraceNo,ChNo}.Mag(1) = 0;
                G1Idx = ([AllFreqSersHT{TraceNo,ChNo}.Mag; 0] > ...
                    AllMinPeaksT(TraceNo,ChNo));
                G1IdxDiff = logical(abs(diff(G1Idx)));
                PeakCnt = sum(G1IdxDiff)/2;
                PeakRegIdxs = find(G1IdxDiff);
                PeaksMagsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksFreqsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksWidthsHT{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksRegsHT{TraceNo,ChNo} = zeros(PeakCnt,2);
                PeaksFreqRegsHT{TraceNo,ChNo} = zeros(PeakCnt,2);
                for PeakNo = 1:PeakCnt
                    Idx1 = PeakRegIdxs(2*PeakNo - 1) + 1;
                    Idx2 = PeakRegIdxs(2*PeakNo);
                    PeaksWidthsHT{TraceNo,ChNo}(PeakNo) = Idx2 - Idx1 + 1;
                    [PeaksMagsHT{TraceNo,ChNo}(PeakNo), idx] = ...
                        max(AllFreqSersHT{TraceNo,ChNo}.Mag(Idx1:Idx2));
                    PeaksFreqsHT{TraceNo,ChNo}(PeakNo) = ...
                        AllFreqSersHT{TraceNo,ChNo}.Freq(idx + Idx1 - 1);
                    PeaksFreqRegsHT{TraceNo,ChNo}(PeakNo,:) = ...
                        [AllFreqSersHT{TraceNo,ChNo}.Freq(Idx1),...
                        AllFreqSersHT{TraceNo,ChNo}.Freq(Idx2)];
                    PeaksRegsHT{TraceNo,ChNo}(PeakNo,:) = [Idx1,Idx2];
                end
            end
        end
    end
    
%     Getting parameters for training
    TrainConf = Conf.Train;
%     Train
    Results.Train = TrainMultiPeak2(TrainConf,ChNamesT,ChHasLenT,...
        PeaksMagsHT,PeaksFreqsHT,PeaksWidthsHT,PeaksRegsHT,PeaksFreqRegsHT);
%%     Prepare data for AnalyzeMultiPeak
	AllFreqSersHA = cell(TraceCntA,ChCntA);
    PeaksMagsHA = cell(TraceCntA,ChCntA);
    PeaksFreqsHA = cell(TraceCntA,ChCntA);
    PeaksWidthsHA = cell(TraceCntA,ChCntA);
    PeaksRegsHA = cell(TraceCntA,ChCntA);
    PeaksFreqRegsHA = cell(TraceCntA,ChCntA);
    
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ChHasLenA(TraceNo,ChNo) == 1
                AllFreqSersHA{TraceNo,ChNo}.MinDist = ...
                    AllFreqSersA{TraceNo,ChNo}.MinDist;
%                     This is not the most efficient way to do this
                RedIdx = (AllFreqSersA{TraceNo,ChNo}.Freq > 0.5);
                AllFreqSersHA{TraceNo,ChNo}.Freq = ...
                    AllFreqSersA{TraceNo,ChNo}.Freq;
                AllFreqSersHA{TraceNo,ChNo}.Mag = ...
                    AllFreqSersA{TraceNo,ChNo}.Mag;
                AllFreqSersHA{TraceNo,ChNo}.Freq(RedIdx) = [];
                AllFreqSersHA{TraceNo,ChNo}.Mag(RedIdx) = [];
                AllFreqSersHA{TraceNo,ChNo}.Mag(1) = 0;
                G1Idx = ([AllFreqSersHA{TraceNo,ChNo}.Mag; 0] > ...
                    AllMinPeaksA(TraceNo,ChNo));
                G1IdxDiff = logical(abs(diff(G1Idx)));
                PeakCnt = sum(G1IdxDiff)/2;
                PeakRegIdxs = find(G1IdxDiff);
                PeaksMagsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksFreqsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksWidthsHA{TraceNo,ChNo} = zeros(PeakCnt,1);
                PeaksRegsHA{TraceNo,ChNo} = zeros(PeakCnt,2);
                PeaksFreqRegsHA{TraceNo,ChNo} = zeros(PeakCnt,2);
                for PeakNo = 1:PeakCnt
                    Idx1 = PeakRegIdxs(2*PeakNo - 1) + 1;
                    Idx2 = PeakRegIdxs(2*PeakNo);
                    PeaksWidthsHA{TraceNo,ChNo}(PeakNo) = Idx2 - Idx1 + 1;
                    [PeaksMagsHA{TraceNo,ChNo}(PeakNo), idx] = ...
                        max(AllFreqSersHA{TraceNo,ChNo}.Mag(Idx1:Idx2));
                    PeaksFreqsHA{TraceNo,ChNo}(PeakNo) = ...
                        AllFreqSersHA{TraceNo,ChNo}.Freq(idx + Idx1 - 1);
                    PeaksFreqRegsHA{TraceNo,ChNo}(PeakNo,:) = ...
                        [AllFreqSersHA{TraceNo,ChNo}.Freq(Idx1),...
                        AllFreqSersHA{TraceNo,ChNo}.Freq(Idx2)];
                    PeaksRegsHA{TraceNo,ChNo}(PeakNo,:) = [Idx1,Idx2];
                end
            end
        end
    end
%     Analyze for MultiPeak
    Results.Analyze = AnalyzeMultiPeak2(...
        Results.Train,ChNamesA,logical(ChHasLenA),...
        PeaksMagsHA,PeaksFreqsHA,PeaksWidthsHA);
%     Analyze the train set for MultiPeak
    
    Results.AnalyzeTrain = AnalyzeMultiPeak2(...
        Results.Train,ChNamesT,logical(ChHasLenT),...
        PeaksMagsHT,PeaksFreqsHT,PeaksWidthsHT);
end
