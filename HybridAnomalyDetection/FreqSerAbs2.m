function FreqSerOut = FreqSerAbs2(FreqSerIn)
    FreqSerOut = FreqSerIn;
    FreqSerOut.Mag = max([FreqSerOut.Mag, FreqSerOut.Mag.^(-1)],[],2);
    FreqSerOut.Mag(isnan(FreqSerOut.Mag)) = 1;
end