DirList = dir('.');
DirList(~[DirList.isdir]) = [];
DirList(1:2) = [];
for Dir = DirList'
    if exist(fullfile('.',Dir.name,[Dir.name '.m']),'file') == 2
        movefile(fullfile('.',Dir.name,[Dir.name '.m']));
        rmdir(Dir.name,'s');
    end
end