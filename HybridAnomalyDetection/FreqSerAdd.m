function FreqSerOut = FreqSerAdd(FreqSerIn1,FreqSerIn2) %#codegen
%     if isnan(FreqSerIn1.Mag)
%         error('First input is NaN.');
%     end
%     if isnan(FreqSerIn2.Mag)
%         error('Second input is NaN.');
%     end
    if isempty(FreqSerIn1.Mag)
        FreqSerOut = FreqSerIn2;
        return
    end
    if isempty(FreqSerIn2.Mag)
        FreqSerOut = FreqSerIn1;
        return
    end
    FreqSerOut.MinDist = lcm(FreqSerIn1.MinDist,FreqSerIn2.MinDist);
    FreqSerOut.Mag = [];
    FreqSerOut.Freq = sort(unique([FreqSerIn1.Freq; FreqSerIn2.Freq]));
%     RealMinDist = 1/(min(diff(FreqSerOut.Freq)));
%     if floor(RealMinDist) > FreqSerOut.MinDist
%         error('Minimum distance mismatch');
%     end
    Idx1 = 1;
    Idx2 = 1;
    IdxOut = 1;
    Len1 = length(FreqSerIn1.Freq);
    Len2 = length(FreqSerIn2.Freq);
    LenOut = length(FreqSerOut.Freq);
    FreqSerOut.Mag = zeros(LenOut,1);
    while IdxOut <= LenOut
        if FreqSerIn1.Freq(Idx1) == FreqSerOut.Freq(IdxOut)
            Val1 = FreqSerIn1.Mag(Idx1);
        elseif FreqSerIn1.Freq(Idx1) < FreqSerOut.Freq(IdxOut)
            Val1 = ...
                (FreqSerOut.Freq(IdxOut)   - FreqSerIn1.Freq(Idx1  ))/...
                (FreqSerIn1.Freq(Idx1 + 1) - FreqSerIn1.Freq(Idx1  ))*...
                 FreqSerIn1.Mag (Idx1 + 1) + ...
                (FreqSerIn1.Freq(Idx1 + 1) - FreqSerOut.Freq(IdxOut))/...
                (FreqSerIn1.Freq(Idx1 + 1) - FreqSerIn1.Freq(Idx1  ))*...
                 FreqSerIn1.Mag (Idx1);
        else
            error ('The Idx in input1 was ahead output idx');
        end
        if FreqSerIn2.Freq(Idx2) == FreqSerOut.Freq(IdxOut)
            Val2 = FreqSerIn2.Mag(Idx2);
        elseif FreqSerIn2.Freq(Idx2) < FreqSerOut.Freq(IdxOut)
            Val2 = ...
                (FreqSerOut.Freq(IdxOut)   - FreqSerIn2.Freq(Idx2  ))/...
                (FreqSerIn2.Freq(Idx2 + 1) - FreqSerIn2.Freq(Idx2  ))*...
                 FreqSerIn2.Mag (Idx2 + 1) + ...
                (FreqSerIn2.Freq(Idx2 + 1) - FreqSerOut.Freq(IdxOut))/...
                (FreqSerIn2.Freq(Idx2 + 1) - FreqSerIn2.Freq(Idx2  ))*...
                 FreqSerIn2.Mag (Idx2);
        else
            error ('The Idx in input2 was ahead output idx');
        end
        FreqSerOut.Mag(IdxOut) = Val1 + Val2;
        IdxOut = IdxOut + 1;
        if Idx1 < Len1
            if FreqSerIn1.Freq(Idx1 + 1) <= FreqSerOut.Freq(IdxOut);
                Idx1 = Idx1 + 1;
            end
        end
        if Idx1 < Len1
            if FreqSerIn1.Freq(Idx1 + 1) <= FreqSerOut.Freq(IdxOut);
                error('Skipped one element inside freq series in 1');
            end
        end
        if Idx2 < Len2
            if FreqSerIn2.Freq(Idx2 + 1) <= FreqSerOut.Freq(IdxOut);
                Idx2 = Idx2 + 1;
            end
        end
        if Idx2 < Len2
            if FreqSerIn2.Freq(Idx2 + 1) <= FreqSerOut.Freq(IdxOut);
                error('Skipped one element inside freq series in 2');
            end
        end
        
    end
end
