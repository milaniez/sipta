function Results = ExistenceClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf)
    [TraceCntT,ChCntT] = size(AllFreqSersT);
    [TraceCntA,ChCntA] = size(AllFreqSersA);
    ChExistsT = ones(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if isnan(AllFreqSersT{TraceNo,ChNo}.MinDist)
                ChExistsT(TraceNo,ChNo) = 0;
            end
        end
    end
    ChExistsA = ones(TraceCntA,ChCntA);  
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if isnan(AllFreqSersA{TraceNo,ChNo}.MinDist)
                ChExistsA(TraceNo,ChNo) = 0;
            end
        end
    end
    ChExType = Conf.Analyze.Type.Text;
    ChExConf = Conf.Analyze.Config;
%     Train for Existence
    Results.Train = TrainExistence(ChNamesT, ChExistsT);
%     Analyze for Existence
    Results.Analyze = AnalyzeExistence(ChExType,ChExConf,Results.Train,ChNamesA,ChExistsA);
end