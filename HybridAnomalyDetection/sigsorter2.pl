use strict;
use warnings;

my $Empty = 0;
my %chTypeFileHash = ();
my @chTypeListFileArray = ();

open (TRACE_INFO,"$ARGV[0]\/trace_info.txt") or die "could not open TRACE_INFO file\n";
my $TraceCnt = <TRACE_INFO>;
close (TRACE_INFO);

open (NAMEFILE,">$ARGV[1]\/type_name.txt") or die "could not open NAMEFILE file\n";

my $chTypeNo = 0;

for (my $TraceNo = 0; $TraceNo < $TraceCnt; $TraceNo++)
{
	print "$ARGV[0]\/trace$TraceNo\/sig_list.txt\n";	open (SIG_LIST,"$ARGV[0]\/trace$TraceNo\/sig_list.txt") or die "could not open SIG_LIST file\n";
	open (SIG_INFO,"$ARGV[0]\/trace$TraceNo\/sig_info.txt") or die "could not open SIG_INFO file\n";
	my $chCnt = <SIG_INFO>;
	close (SIG_INFO);
	for (my $chNo = 0; $chNo < $chCnt; $chNo++)
	{
		my $chType = <SIG_LIST>;
		$chType =~ /^\w*: *(.*?)\n$/;
		if (! exists $chTypeFileHash{$1})
		{
			$chTypeFileHash{$1} = $chTypeNo;
			open (my $chTypeListFile,">$ARGV[1]\/type_list$chTypeNo.txt") or die "could not open output file\n";
			push (@chTypeListFileArray, $chTypeListFile);
			NAMEFILE -> print ("$1\n");
			$chTypeNo++;
		}
		$chTypeListFileArray[$chTypeFileHash{$1}] -> print ("$TraceNo  $chNo\n");
	}
}
close (NAMEFILE);
open (INFOFILE,">$ARGV[1]\/type_info.txt") or die "could not open INFOFILE file\n";
INFOFILE -> print ("$chTypeNo");
close (INFOFILE);

for (my $i = 0; $i < $chTypeNo; $i++)
{
	close ($chTypeListFileArray[$i]);
}