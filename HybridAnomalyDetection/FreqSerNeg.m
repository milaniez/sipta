function FreqSerOut = FreqSerNeg(FreqSerIn)
    FreqSerOut = FreqSerIn;
    FreqSerOut.Mag = -FreqSerOut.Mag;
end