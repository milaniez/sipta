function FreqSerOut = FreqSerStd(FreqSersIn, FreqSerPat)
    FreqSerPat.Mag = zeros(size(FreqSerPat.Mag));
    FreqSerInCnt = length(FreqSersIn);
    for FreqSerNo = 1:FreqSerInCnt
        FreqSersIn{FreqSerNo} = FreqSerAdd(FreqSersIn{FreqSerNo},FreqSerPat);
    end
    FreqSerOut = FreqSerPat;
    for Idx = 1:length(FreqSerPat.Mag)
        Mag = zeros(FreqSerInCnt,1);
        for FreqSerNo = 1:FreqSerInCnt
            Mag(FreqSerNo) = FreqSersIn{FreqSerNo}.Mag(Idx);
        end
        FreqSerOut.Mag(Idx) = std(Mag);
    end
end