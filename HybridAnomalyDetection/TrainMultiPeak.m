function TrainData = TrainMultiPeak(Type,Config,ChNames,ChHasLen,...
        PeaksMags,PeaksFreqs,PeaksWidths)
    if strcmp(Type,'FreqKMean')
        MaxPeakCnt = str2double(Config.PeakCnt.Text);
        MaxPresPeakCnt = str2double(Config.MaxPresPeakCntRatio.Text)*MaxPeakCnt;
        [~,ChCnt] = size(ChHasLen);
        MagMean = cell(1,ChCnt);
        MagStd = cell(1,ChCnt);
        FreqMean = cell(1,ChCnt);
        FreqStd = cell(1,ChCnt);
        InvalidChNos = [];
        for ChNo = 1:ChCnt
            CellsLens = cellfun(@length,PeaksFreqs(:,ChNo));
            EmptyCells = (CellsLens == 0);
            OverPresPeakCells = (CellsLens > MaxPresPeakCnt);
            InvalidCells = EmptyCells | OverPresPeakCells;
            InvalidCellCnt = sum(InvalidCells);
            ValidCellCnt = length(PeaksFreqs(:,ChNo)) - InvalidCellCnt;
            if ValidCellCnt <= 1
                InvalidChNos = [InvalidChNos ChNo]; %#ok<AGROW>
                continue
            end
            ChPeaksFreqs = PeaksFreqs(~InvalidCells,ChNo);
            ChPeaksMags = PeaksMags(~InvalidCells,ChNo);
            ChPeaksWidths = PeaksWidths(~InvalidCells,ChNo);
            PeakCnt = max(cellfun(@length,PeaksFreqs(:,ChNo)));
            if PeakCnt > MaxPeakCnt
                PeakCnt = MaxPeakCnt;
            end
            ValidTraceCnt = length(ChPeaksFreqs);
            for TraceNo = 1:ValidTraceCnt
                if length(ChPeaksFreqs{TraceNo}) <= PeakCnt
                    continue
                end
                [~, idx] = sort(ChPeaksMags{TraceNo},'descend');
                idx = idx(1:PeakCnt);
                ChPeaksMags{TraceNo} = ChPeaksMags{TraceNo}(idx);
                ChPeaksFreqs{TraceNo} = ChPeaksFreqs{TraceNo}(idx);
                ChPeaksWidths{TraceNo} = ChPeaksWidths{TraceNo}(idx);
            end
            AllChPeaksMags = cell2mat(ChPeaksMags);
            AllChPeaksFreqs = cell2mat(ChPeaksFreqs);
            AllChPeaksWidths = cell2mat(ChPeaksWidths);
            ClustersIdx = kmeans(AllChPeaksFreqs,PeakCnt);
            MagMean{ChNo} = cell(PeakCnt,1);
            MagStd{ChNo} = cell(PeakCnt,1);
            FreqMean{ChNo} = cell(PeakCnt,1);
            FreqStd{ChNo} = cell(PeakCnt,1);
            InvalidClusterNos = [];
            for ClusterNo = 1:PeakCnt
                ClusterIdx = (ClustersIdx == ClusterNo);
                if sum(ClusterIdx) <= 1
                    InvalidClusterNos = [InvalidClusterNos ClusterNo]; %#ok<AGROW>
                    continue
                end
                ClusterChPeaksMags = AllChPeaksMags(ClusterIdx);
                ClusterChPeaksFreqs = AllChPeaksFreqs(ClusterIdx);
                ClusterChPeaksWidths = AllChPeaksWidths(ClusterIdx); %#ok<NASGU>
                MagMean{ChNo}{ClusterNo} = mean(ClusterChPeaksMags);
                MagStd{ChNo}{ClusterNo} = std(ClusterChPeaksMags);
                FreqMean{ChNo}{ClusterNo} = mean(ClusterChPeaksFreqs);
                FreqStd{ChNo}{ClusterNo} = std(ClusterChPeaksFreqs);
            end
            MagMean{ChNo}(InvalidClusterNos) = [];
            MagMean{ChNo} = cell2mat(MagMean{ChNo});
            MagStd{ChNo}(InvalidClusterNos) = [];
            MagStd{ChNo} = cell2mat(MagStd{ChNo});
            FreqMean{ChNo}(InvalidClusterNos) = [];
            FreqMean{ChNo} = cell2mat(FreqMean{ChNo});
            FreqStd{ChNo}(InvalidClusterNos) = [];
            FreqStd{ChNo} = cell2mat(FreqStd{ChNo});
        end
        TrainData.ChNames = ChNames;
        TrainData.ChNames(InvalidChNos) = [];
        MagMean(InvalidChNos) = [];
        TrainData.MagMean = MagMean;
        MagStd(InvalidChNos) = [];
        TrainData.MagStd = MagStd;
        FreqMean(InvalidChNos) = [];
        TrainData.FreqMean = FreqMean;
        FreqStd(InvalidChNos) = [];
        TrainData.FreqStd = FreqStd;
    elseif strcmp(Type,'FreqHierarchical')
        MaxPeakCnt = str2num(Config.PeakCnt.Text); %#ok<ST2NM>
        MaxPresPeakCnt = str2double(Config.MaxPresPeakCntRatio.Text)*MaxPeakCnt;
        [~,ChCnt] = size(ChHasLen);
        MagMean = cell(1,ChCnt);
        MagStd = cell(1,ChCnt);
        FreqMean = cell(1,ChCnt);
        FreqStd = cell(1,ChCnt);
        InvalidChNos = [];
        for ChNo = 1:ChCnt
            CellsLens = cellfun(@length,PeaksFreqs(:,ChNo));
            EmptyCells = (CellsLens == 0);
            OverPresPeakCells = (CellsLens > MaxPresPeakCnt);
            InvalidCells = EmptyCells | OverPresPeakCells;
            InvalidCellCnt = sum(InvalidCells);
            ValidCellCnt = length(PeaksFreqs(:,ChNo)) - InvalidCellCnt;
            if ValidCellCnt <= 1
                InvalidChNos = [InvalidChNos ChNo]; %#ok<AGROW>
                continue
            end
            ChPeaksFreqs = PeaksFreqs(~InvalidCells,ChNo);
            ChPeaksMags = PeaksMags(~InvalidCells,ChNo);
            ChPeaksWidths = PeaksWidths(~InvalidCells,ChNo);
            PeakCnt = max(cellfun(@length,PeaksFreqs(:,ChNo)));
            if PeakCnt > MaxPeakCnt
                PeakCnt = MaxPeakCnt;
            end
            ValidTraceCnt = length(ChPeaksFreqs);
            for TraceNo = 1:ValidTraceCnt
                if length(ChPeaksFreqs{TraceNo}) <= PeakCnt
                    continue
                end
                [~, idx] = sort(ChPeaksMags{TraceNo},'descend');
                idx = idx(1:PeakCnt);
                ChPeaksMags{TraceNo} = ChPeaksMags{TraceNo}(idx);
                ChPeaksFreqs{TraceNo} = ChPeaksFreqs{TraceNo}(idx);
                ChPeaksWidths{TraceNo} = ChPeaksWidths{TraceNo}(idx);
            end
            AllChPeaksMags = cell2mat(ChPeaksMags);
            AllChPeaksFreqs = cell2mat(ChPeaksFreqs);
            AllChPeaksWidths = cell2mat(ChPeaksWidths);
            ClustersIdx = kmeans(AllChPeaksFreqs,PeakCnt);
            MagMean{ChNo} = cell(PeakCnt,1);
            MagStd{ChNo} = cell(PeakCnt,1);
            FreqMean{ChNo} = cell(PeakCnt,1);
            FreqStd{ChNo} = cell(PeakCnt,1);
            InvalidClusterNos = [];
            for ClusterNo = 1:PeakCnt
                ClusterIdx = (ClustersIdx == ClusterNo);
                if sum(ClusterIdx) <= 1
                    InvalidClusterNos = [InvalidClusterNos ClusterNo]; %#ok<AGROW>
                    continue
                end
                ClusterChPeaksMags = AllChPeaksMags(ClusterIdx);
                ClusterChPeaksFreqs = AllChPeaksFreqs(ClusterIdx);
                ClusterChPeaksWidths = AllChPeaksWidths(ClusterIdx); %#ok<NASGU>
                MagMean{ChNo}{ClusterNo} = mean(ClusterChPeaksMags);
                MagStd{ChNo}{ClusterNo} = std(ClusterChPeaksMags);
                FreqMean{ChNo}{ClusterNo} = mean(ClusterChPeaksFreqs);
                FreqStd{ChNo}{ClusterNo} = std(ClusterChPeaksFreqs);
            end
            MagMean{ChNo}(InvalidClusterNos) = [];
            MagMean{ChNo} = cell2mat(MagMean{ChNo});
            MagStd{ChNo}(InvalidClusterNos) = [];
            MagStd{ChNo} = cell2mat(MagStd{ChNo});
            FreqMean{ChNo}(InvalidClusterNos) = [];
            FreqMean{ChNo} = cell2mat(FreqMean{ChNo});
            FreqStd{ChNo}(InvalidClusterNos) = [];
            FreqStd{ChNo} = cell2mat(FreqStd{ChNo});
        end
        TrainData.ChNames = ChNames;
        TrainData.ChNames(InvalidChNos) = [];
        MagMean(InvalidChNos) = [];
        TrainData.MagMean = MagMean;
        MagStd(InvalidChNos) = [];
        TrainData.MagStd = MagStd;
        FreqMean(InvalidChNos) = [];
        TrainData.FreqMean = FreqMean;
        FreqStd(InvalidChNos) = [];
        TrainData.FreqStd = FreqStd;
    else
        error('Invalid Option');
    end
end