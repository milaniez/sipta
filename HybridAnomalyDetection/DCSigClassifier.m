function Results = DCSigClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf)
    [TraceCntT,ChCntT] = size(AllFreqSersT);
    [TraceCntA,ChCntA] = size(AllFreqSersA);
    ChHasLenT = zeros(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ~isnan(AllFreqSersT{TraceNo,ChNo}.MinDist) && ...
                    AllFreqSersT{TraceNo,ChNo}.MinDist ~= 0
                ChHasLenT(TraceNo,ChNo) = 1;
            end
        end
    end
    ChHasLenA = zeros(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ~isnan(AllFreqSersA{TraceNo,ChNo}.MinDist) && ...
                    AllFreqSersA{TraceNo,ChNo}.MinDist ~= 0
                ChHasLenA(TraceNo,ChNo) = 1;
            end
        end
    end
%     Prepare data for TrainDC function
    ChDCsT = NaN(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ChHasLenT(TraceNo,ChNo)
%                 This only works for uniform initial signals. If the freq
%                 series is actually non-uniform, this simple formula does
%                 not work properly
                ChDCsT(TraceNo,ChNo) = ...
                    AllFreqSersT{TraceNo,ChNo}.Mag(1)/...
                    AllFreqSersT{TraceNo,ChNo}.MinDist;
            end
        end
    end
%     Train for DC Significance
    Results.Train = TrainDC(ChNamesT, logical(ChHasLenT), ChDCsT);
%     Get Config for AnalyzeDC
    DCSigType = Conf.Analyze.Type.Text;
    DCSigConf = Conf.Analyze.Config;
%     Prepare analyze data
    ChDCsA = NaN(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ChHasLenA(TraceNo,ChNo)
%                 This only works for uniform initial signals. If the freq
%                 series is actually non-uniform, this simple formula does
%                 not work properly
                ChDCsA(TraceNo,ChNo) = ...
                    AllFreqSersA{TraceNo,ChNo}.Mag(1)/...
                    AllFreqSersA{TraceNo,ChNo}.MinDist;
            end
        end
    end
    Results.Analyze = ...
        AnalyzeDC(DCSigType,DCSigConf,Results.Train,ChNamesA,...
        logical(ChHasLenA),ChDCsA);
end