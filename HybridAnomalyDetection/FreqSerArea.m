function Area = FreqSerArea(FreqSer)
    MagMean = (FreqSer.Mag + circshift(FreqSer.Mag,-1))/2;
    MagMean(length(MagMean)) = [];
    FreqDiff = diff(FreqSer.Freq);
    Area = sum(FreqDiff.*MagMean);
end