function AnalyzeResults = AnalyzeSpecLikeli(Type,Config,...
        TrainData,ChNames,ChHasLen,AllEnvs)
    
    if strcmp(Type,'MinStdRatioSum')
        [TraceCnt,ChCnt] = size(ChHasLen); %#ok<ASGLU>
        AnalyzeResults = cell(TraceCnt,1);
        ScoreThreshold = str2num(Config.ScoreThreshold.Text); %#ok<NASGU,ST2NM>
        for TraceNo = 1:TraceCnt
            TraceChNames = ChNames(ChHasLen(TraceNo,:));
            TraceEnvs = AllEnvs(TraceNo,ChHasLen(TraceNo,:));
            [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
            TrainEnvAvg = TrainData.EnvAvg(TrainChIdx);
            TrainEnvStd= TrainData.EnvStd(TrainChIdx);
            TraceEnvs = TraceEnvs(AnalyzeChIdx);
            ValidChCnt = length(TraceEnvs);
            AnalyzeResults{TraceNo}.ChScores = zeros(ValidChCnt,1);
            AnalyzeResults{TraceNo}.TraceTotalEnvLens = 0;
            AnalyzeResults{TraceNo}.TraceEnvLens = zeros(ValidChCnt,1);
            for ChNo = 1:ValidChCnt
                ChTrainEnvAvg = TrainEnvAvg{ChNo};
                ChTrainEnvStd = TrainEnvStd{ChNo};
                ClustCnt = length(TrainEnvAvg{ChNo});
                ChScore = inf;
                for ClustNo = 1:ClustCnt
                    ClustChSubScore = FreqSerAbs(FreqSerDiv(FreqSerSub(...
                        TraceEnvs{ChNo},ChTrainEnvAvg{ClustNo}),ChTrainEnvStd{ClustNo}));
                    ClustChSubScore.Mag(isnan(ClustChSubScore.Mag)) = 1;
                    ClustChSubScore.Mag(isinf(ClustChSubScore.Mag)) = 10;
                    ChScore = min(ChScore,FreqSerArea(ClustChSubScore));
                    if ChScore == sum(FreqSerArea(ClustChSubScore))
                        ChEnvLen = length(ClustChSubScore.Mag);
                    end
                end
                AnalyzeResults{TraceNo}.ChScores(ChNo) = ChScore;
                AnalyzeResults{TraceNo}.TraceEnvLens(ChNo) = ChEnvLen;
                AnalyzeResults{TraceNo}.TraceTotalEnvLens = ...
                     AnalyzeResults{TraceNo}.TraceTotalEnvLens + ChEnvLen;
            end
            AnalyzeResults{TraceNo}.Score = ...
                sum(AnalyzeResults{TraceNo}.ChScores)/...
                AnalyzeResults{TraceNo}.TraceTotalEnvLens;
        end
    else
        error('Invalid option');
    end
end