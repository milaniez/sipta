use strict;
use warnings;
use IPC::System::Simple;

my $TrainCSVDir = $ARGV[0];
my $TrainDir = $ARGV[1];
my $AnalyzeCSVDir = $ARGV[2];
my $AnalyzeDir = $ARGV[3];

mkdir ($TrainDir."\\Output");
mkdir ($TrainDir."\\sortdata");
mkdir ($AnalyzeDir."\\Output");
mkdir ($AnalyzeDir."\\sortdata");

my @ARGV4TrainRunner = ($TrainCSVDir,"$TrainDir\\Output",$ARGV[4],$ARGV[5]);
my @ARGV4AnalyzeRunner = ($AnalyzeCSVDir,"$AnalyzeDir\\Output",$ARGV[4],$ARGV[5]);

my @ARGV4TrainSorter = ("$TrainDir\\Output","$TrainDir\\sortdata");
my @ARGV4AnalyzeSorter = ("$AnalyzeDir\\Output","$AnalyzeDir\\sortdata");

system($^X,"runner.pl",@ARGV4TrainRunner);
system($^X,"sigsorter2.pl",@ARGV4TrainSorter);
system($^X,"runner.pl",@ARGV4AnalyzeRunner);
system($^X,"sigsorter2.pl",@ARGV4AnalyzeSorter);

system("matlab -r addpath('%CD%');SiPTA('$TrainDir\\Output','$TrainDir\\sortdata','$AnalyzeDir\\Output','$AnalyzeDir\\sortdata','entry.txt',0.05,'$ARGV[6]');exit");
