use strict;
use warnings;

open (SIGINFO, "<$ARGV[0]\/sig_info.txt");
my $SigCnt = <SIGINFO>;
close (SIGINFO);

for (my $SigNo = 0; $SigNo < $SigCnt; $SigNo++)
{
	open (CHCLASSINFO, "<$ARGV[0]\/sig$SigNo\/chclassinfo.txt");
	open (SIGINFO,     "<$ARGV[0]\/sig$SigNo\/siginfo.txt");
	my $chClassCnt = <CHCLASSINFO>;
	my $SigLen = <SIGINFO>;
	close CHCLASSINFO;
	close SIGINFO;
	
	for (my $chClassNo = 0; $chClassNo < $chClassCnt; $chClassNo++)
	{
		my %chHash = ();
		my $chCnt = 0;
		
		my @chFileList = ();
		my @tFileList = ();
		my @entryFileList = ();
		
		my $Line = 0;
		
		my $tLine = 0;
		my $entryLine = 0;
		
		open (CHCLASSFILE, "<$ARGV[0]\/sig$SigNo\/chClass$chClassNo\/chclass.txt");
		# print "ARGV[0]\/sig$SigNo\/chClass$chClassNo\/chclass.txt\n";
		open (TFILE, "<$ARGV[0]\/sig$SigNo\/t.txt");
		# print "ARGV[0]\/sig$SigNo\/t.txt\n";
		open (ENTRYFILE, "<$ARGV[0]\/sig$SigNo\/entry.txt");
		# print "ARGV[0]\/sig$SigNo\/entry.txt\n";
		open (CHLIST, ">$ARGV[0]\/sig$SigNo\/chClass$chClassNo\/chlist.txt");
		
		for (my $LineNo = 0; $LineNo < $SigLen; $LineNo++)
		{
			$Line = <CHCLASSFILE>;
			$tLine = <TFILE>;
			$entryLine = <ENTRYFILE>;
			if (! exists $chHash{$Line})
			{
				$chHash{$Line} = $chCnt;
				open (my $chFile, ">$ARGV[0]\/sig$SigNo\/chClass$chClassNo\/ch$chCnt.txt");
				push (@chFileList, $chFile);
				open (my $tFile, ">$ARGV[0]\/sig$SigNo\/chClass$chClassNo\/t$chCnt.txt");
				push (@tFileList, $tFile);
				open (my $entryFile, ">$ARGV[0]\/sig$SigNo\/chClass$chClassNo\/entry$chCnt.txt");
				push (@entryFileList, $entryFile);
				CHLIST -> print ("$chCnt: $Line");
				$chCnt++;
			}
			my $chNo = $chHash{$Line};
			$chFileList[$chNo] -> print ("$LineNo\n");
			$tFileList[$chNo] -> print ("$tLine");
			$entryFileList[$chNo] -> print ("$entryLine");
		}
		
		close (CHLIST);
		close (ENTRYFILE);
		close (TFILE);
		close (CHCLASSFILE);
	}
}