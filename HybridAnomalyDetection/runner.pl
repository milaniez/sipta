use strict;
use warnings;
use IPC::System::Simple qw(system capture);

open (TRACELIST, ">$ARGV[1]"."\/"."trace_list.txt") or die "could not create trace list file\n";
open (TRACEINFO, ">$ARGV[1]"."\/"."trace_info.txt") or die "could not create trace info file\n";
opendir (DIR, "$ARGV[0]") or die "could not get input directory\n";
my @DirContent = readdir(DIR);
closedir (DIR);

my $Iteration = 0;
foreach my $DirItem (@DirContent)
{
	if ($DirItem =~ /.*\.csv$/i)
	{
		my @ARGV2 = @ARGV;
		$ARGV2[0] = "$ARGV[0]"."\/"."$DirItem";
		
		TRACELIST -> print ("$DirItem\n");
		mkdir      ("$ARGV[1]"."\/"."trace$Iteration");
		$ARGV2[1] = "$ARGV[1]"."\/"."trace$Iteration";
		
		system ($^X,"trace2sigs.pl",@ARGV2);
		system ($^X,"sigs2chclasses.pl",@ARGV2);
		system ($^X,"chclasses2chs.pl",@ARGV2);
		
		$Iteration++;
	}
}

close (TRACELIST);

TRACEINFO -> print ("$Iteration");
close (TRACEINFO);