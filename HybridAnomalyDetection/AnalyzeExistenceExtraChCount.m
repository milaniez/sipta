function AnalyzeResults = AnalyzeExistenceExtraChCount(TrainData,Config,ChNames,ChExists)
    [TraceCnt,~] = size(ChExists);
    AnalyzeResults = cell(TraceCnt,1);
    ChInTrainScore = str2num(Config.ChInTrainScore.Text); %#ok<ST2NM>
    ChInAnalyzeScore = str2num(Config.ChInAnalyzeScore.Text); %#ok<ST2NM>
    ScoreThreshold = str2num(Config.ScoreThreshold.Text); %#ok<ST2NM>
    TrainPresistentIdxs = (TrainData.ExistenceProb == 1);
    TrainPresistentChNames = TrainData.ChNames(TrainPresistentIdxs);
    for TraceNo = 1:TraceCnt
        PresentChNames = ChNames(logical(ChExists(TraceNo,:)));
        Score = length(setdiff(TrainPresistentChNames,PresentChNames))*...
            ChInTrainScore + ...
            length(setdiff(PresentChNames,TrainData.ChNames))*...
            ChInAnalyzeScore;
        AnalyzeResults{TraceNo}.Score = Score;
        AnalyzeResults{TraceNo}.Label = (Score > ScoreThreshold);
        AnalyzeResults{TraceNo}.ScoreRatio = Score/ScoreThreshold;
        AnalyzeResults{TraceNo}.TrainExtra = ...
            length(setdiff(TrainPresistentChNames,PresentChNames));
        AnalyzeResults{TraceNo}.AnalyzeExtra = ...
            length(setdiff(PresentChNames,TrainData.ChNames));
    end
end