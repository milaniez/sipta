Scenarios = {'s1','s2','s3','s4'};
CleanIdx = {...
    1:5;
    1:2;
    1:6;
    1:5};
DirtyIdx = {...
    6:10;
    3:6;
    7:11;
    6:10};
for i = 1:4
    for j = 1:length(Names{i})
        Plotter(Results{i}{j}.Analyze,[Scenarios{i} ' - ' Names{i}{j}],...
            CleanIdx{i},DirtyIdx{i},'Figures');
    end
end