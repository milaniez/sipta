function [Results, Names, TrainTraceNames, AnalyzeTraceNames] = ...
        SiPTAMainCanCar (Input,ConfigFile)
%%  Initialize
%     Get and prepare input from Input :)
    Config = xml2struct(ConfigFile);
    PreProcConfig = Config.All.PreProc;
    Config = Config.All.Proc;
    TraceNames = Input.TraceNames;
    CleanNamePattern = PreProcConfig.DataSettings.CleanNamePattern.Text;
    AllSers = Input.AllSers;
    CleanTraceNames = cell(0,1);
    DirtyTraceNames = cell(0,1);
    TraceCnt = length(TraceNames);
    for TraceNo = 1:TraceCnt
        if isempty(regexp(TraceNames{TraceNo},CleanNamePattern, 'once'))
            DirtyTraceNames = [DirtyTraceNames; TraceNames(TraceNo)];
        else
            CleanTraceNames = [CleanTraceNames; TraceNames(TraceNo)];
        end
    end
    [~,CleanIdx,~] = intersect(TraceNames,CleanTraceNames);
    [~,DirtyIdx,~] = intersect(TraceNames,DirtyTraceNames);
    AllCleanSers = AllSers(CleanIdx);
    AllDirtySers = AllSers(DirtyIdx);
%     Determine Train and Analyze traces
    TrainTraceNos = strsplit(Config.TrainTraces.Text,':');
    TrainTraceNos = sort(cellfun(@str2double,TrainTraceNos))';
    TrainTraceNames = CleanTraceNames(TrainTraceNos);
    [~,iTrainTraceNames,iCleanTrainTrace] = ...
        intersect(TrainTraceNames,CleanTraceNames);
    if length(iCleanTrainTrace) ~= length(TrainTraceNames)
        error('Non-existent train trace names');
    end
    [CleanAnalyzeTraceNames,iCleanAnalyzeTrace] = ...
        setdiff(CleanTraceNames,TrainTraceNames);
    TrainTraceNames = TrainTraceNames(iTrainTraceNames);
    AnalyzeTraceNames = [CleanAnalyzeTraceNames;DirtyTraceNames];
    AllTrainSers = AllCleanSers(iCleanTrainTrace);
    AllAnalyzeSers = [AllCleanSers(iCleanAnalyzeTrace);AllDirtySers];
    ChNamesT = cell(0,1);
    TraceCntT = length(AllTrainSers);
    for TraceNo = 1:TraceCntT
        ChNamesT = [ChNamesT;AllTrainSers{TraceNo}.ChNames]; %#ok<AGROW>
    end
    ChNamesT = sort(unique(ChNamesT));
    ChCntT = length(ChNamesT);
    AllFreqSersT = cell(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            ChName = ChNamesT{ChNo};
            ChIdx = find(ismember(AllTrainSers{TraceNo}.ChNames,ChName));
            if isempty(ChIdx)
                AllFreqSersT{TraceNo,ChNo}.Mag = nan;
                AllFreqSersT{TraceNo,ChNo}.Freq = nan;
                AllFreqSersT{TraceNo,ChNo}.MinDist = nan;
            else
                AllFreqSersT{TraceNo,ChNo} = ...
                    AllTrainSers{TraceNo}.FreqSers{ChIdx};
            end
        end
    end
    ChNamesA = cell(0,1);
    TraceCntA = length(AllAnalyzeSers);
    for TraceNo = 1:TraceCntA
        ChNamesA = [ChNamesA;AllAnalyzeSers{TraceNo}.ChNames]; %#ok<AGROW>
    end
    ChNamesA = sort(unique(ChNamesA));
    ChCntA = length(ChNamesA);
    AllFreqSersA = cell(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            ChName = ChNamesA{ChNo};
            ChIdx = find(ismember(AllAnalyzeSers{TraceNo}.ChNames,ChName));
            if isempty(ChIdx)
                AllFreqSersA{TraceNo,ChNo}.Mag = nan;
                AllFreqSersA{TraceNo,ChNo}.Freq = nan;
                AllFreqSersA{TraceNo,ChNo}.MinDist = nan;
            else
                AllFreqSersA{TraceNo,ChNo} = ...
                    AllAnalyzeSers{TraceNo}.FreqSers{ChIdx};
            end
        end
    end
%%    Find different levels
    LevelCnt = str2double(Config.Levels.LevelCnt.Text);
    Names = cell(LevelCnt,1);
    for LevelNo = 1:LevelCnt
        Names{LevelNo} = ...
            Config.Levels.(['LevelNo' num2str(LevelNo)]).Text;
    end
%%    Perform different levels
    Results = cell(LevelCnt,1);
    for LevelNo = 1:LevelCnt
        Conf = Config.(['LevelNo' num2str(LevelNo)]);
        switch Names{LevelNo}
            case 'ChannelExistence'
                Results{LevelNo} = ExistenceClassifier...
                    (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf);
            case 'ChannelLength'
                Results{LevelNo} = LengthClassifier...
                    (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf);
            case 'DCSignificance'
                Results{LevelNo} = DCSigClassifier...
                    (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf);
            case 'MultiPeak'
                NormOp = PreProcConfig.SpecSettings.SpecOption.Text;
                Results{LevelNo} = MultiPeakClassifier...
                    (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf,NormOp);
            case 'SpectrumLikelihood'
                Results{LevelNo} = SpecLikeliClassifier...
                    (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf);
        end
    end
    
end