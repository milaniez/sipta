function AnalyzeResults = AnalyzeExistence(Type,Config,TrainData,ChNames,ChExists)
    if strcmp(Type,'ExtraChCount')
        AnalyzeResults = AnalyzeExistenceExtraChCount(TrainData,Config,ChNames,ChExists);
    else
        error('Invalid option');
    end
end