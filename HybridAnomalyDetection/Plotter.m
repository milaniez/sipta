function Plotter(AnalyzeResults,Title,CleanIdx,DirtyIdx,Folder)
    figure;
    FontSize = 10;
    MarkerSize = 10;
    YVal = 1:length(CleanIdx);
    XVal = zeros(length(CleanIdx),1);
    for i = 1:length(XVal)
        XVal(i) = AnalyzeResults{CleanIdx(i)}.Score;
    end
    semilogx(XVal,YVal,'d','Marker','s',...
        'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',MarkerSize);
    
    hold on;
    
    YVal = (length(CleanIdx) + 1):length(AnalyzeResults);
    XVal = zeros(length(DirtyIdx),1);
    for i = 1:length(XVal)
        XVal(i) = AnalyzeResults{DirtyIdx(i)}.Score;
    end
    semilogx(XVal,YVal,'d','Marker','>',...
        'MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',MarkerSize);
    
    LegendHandle = legend('Clean','Dirty');
    set(LegendHandle,'Location','SouthEast','FontSize',FontSize);
    
    title([Title ' - Scores'],'FontSize',FontSize);
    xlabel('Score Value','FontSize',FontSize);
    ylabel('Trace Number','FontSize',FontSize);
    
    print([Folder '\' Title],'-dpdf');
    close
end