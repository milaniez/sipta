function Results = SiPTAInitCanCar (ConfigFile)
%%  Initialize
%     Read Config File
    Config = xml2struct(ConfigFile);
    TraceDir = Config.All.PreProc.DataSettings.TraceDir.Text;
    Wildcard = Config.All.PreProc.DataSettings.Wildcard.Text;
    ChColsNames = strsplit(Config.All.PreProc.SpecSettings.ChCols.Text,'-');
    TraceList = dir(fullfile(TraceDir,Wildcard));
    TraceList([TraceList(:).isdir] == true) = [];
    TraceCnt = length(TraceList);
    AllSers = cell(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        TraceNo
        TraceTable = readtable(fullfile(TraceDir,...
            TraceList(TraceNo).name),'Format','%f%s%s%s%s%s%s%s%s%s%s%s%s%s%s');
        ColNames = TraceTable.Properties.VariableNames;
        [~,ColNos,~] = intersect(ColNames,ChColsNames);
        ColNos = sort(ColNos);
        TraceTable = table2cell(TraceTable);
        TimeCol = cell2mat(TraceTable(:,1));
        ChCols = TraceTable(:,ColNos);
        [EntryCnt,~] = size(ChCols);
        ChCol = cell(EntryCnt,1);
        for EntryNo = 1:EntryCnt
            ChCol{EntryNo} = strjoin(ChCols(EntryNo,:),'::');
        end
        [ChNames,~,RowIdx] = unique(ChCol);
        ChCnt = length(ChNames);
        TraceTimeSers = cell(ChCnt,1);
        TraceFreqSers = cell(ChCnt,1);
        for ChNo = 1:ChCnt
            TimeStmps = TimeCol(RowIdx == ChNo);
            TimeSer = diff(TimeStmps);
            TraceTimeSers{ChNo} = double(TimeSer);
            FreqSer = abs(fft(TraceTimeSers{ChNo}));
            FreqSerLen = length(FreqSer);
            if ~isempty(FreqSer)
                FreqSer(1) = FreqSer(1)/2;
                TraceFreqSers{ChNo}.Mag = [FreqSer; FreqSer(1)];
                TraceFreqSers{ChNo}.Freq = (0:1/FreqSerLen:1)';
                TraceFreqSers{ChNo}.MinDist = FreqSerLen;
            else
                TraceFreqSers{ChNo}.Mag = []';
                TraceFreqSers{ChNo}.Freq = []';
                TraceFreqSers{ChNo}.MinDist = 0;
            end
        end
        AllSers{TraceNo}.TimeSers = TraceTimeSers;
        AllSers{TraceNo}.FreqSers = TraceFreqSers;
        AllSers{TraceNo}.ChNames = ChNames;
    end
%%  Return Results
    Results.TraceNames = {TraceList.name}';
    Results.AllSers = AllSers;
end