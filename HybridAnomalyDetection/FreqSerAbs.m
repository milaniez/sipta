function FreqSerOut = FreqSerAbs(FreqSerIn)
    FreqSerOut = FreqSerIn;
    FreqSerOut.Mag = abs(FreqSerOut.Mag);
end