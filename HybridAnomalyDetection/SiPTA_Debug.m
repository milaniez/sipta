function SiPTA_Debug (ConfigFile,TOutput,AOutput)
    Input = SiPTAInit (ConfigFile);
    [Results,Names,TNames,ANames] = SiPTAMain(Input,ConfigFile);
    SaveResults (Results,Names,TNames,ANames,TOutput,AOutput);
end
