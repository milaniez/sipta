function TrainData = TrainSpecLikeli(ChNames, ChHasLen, AllEnvClust, AllClustTraceNos, AllEnv)
    [TraceCnt,ChCnt] = size(ChHasLen);
    TrainData.TraceCnt = TraceCnt;
    TrainData.OrigChCnt = ChCnt;
    TrainData.OrigChNames = ChNames;
    ValidData = false(ChCnt,1);
    TrainData.EnvAvg = cell(ChCnt,1);
    TrainData.EnvStd = cell(ChCnt,1);
    for ChNo = 1:ChCnt
        if isnan(AllEnvClust{ChNo}) ~= 1
            ValidData(ChNo) = true;
            ClustCnt = length(unique(AllEnvClust{ChNo}));
            TrainData.EnvAvg{ChNo} = cell(ClustCnt,1);
            TrainData.EnvStd{ChNo} = cell(ClustCnt,1);
            for ClustNo = 1:ClustCnt
                ClustTraceNos = AllClustTraceNos{ChNo}(AllEnvClust{ChNo} == ClustNo);
                ClustEnvs = AllEnv(ClustTraceNos,ChNo);
                TrainData.EnvAvg{ChNo}{ClustNo} = FreqSerMean(ClustEnvs);
                TrainData.EnvStd{ChNo}{ClustNo} = FreqSerStd(ClustEnvs,TrainData.EnvAvg{ChNo}{ClustNo});
            end
        end
    end
    TrainData.ChNames = ChNames(ValidData);
    TrainData.ChCnt = length(TrainData.ChNames);
    TrainData.EnvAvg = TrainData.EnvAvg(ValidData);
    TrainData.EnvStd = TrainData.EnvStd(ValidData);
end