function Results = LengthClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf)
    [TraceCntT,ChCntT] = size(AllFreqSersT);
    [TraceCntA,ChCntA] = size(AllFreqSersA);
   ChExistsT = ones(TraceCntT,ChCntT);  
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if isnan(AllFreqSersT{TraceNo,ChNo}.MinDist)
                ChExistsT(TraceNo,ChNo) = 0;
            end
        end
    end
    ChExistsA = ones(TraceCntA,ChCntA);  
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if isnan(AllFreqSersA{TraceNo,ChNo}.MinDist)
                ChExistsA(TraceNo,ChNo) = 0;
            end
        end
    end
    ChLengthsT = zeros(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            ChLengthsT(TraceNo,ChNo) = ...
                length(AllFreqSersT{TraceNo,ChNo}.Mag);
            if ChLengthsT(TraceNo,ChNo) == 0
                ChLengthsT(TraceNo,ChNo) = 1;
            end
        end
    end
    ChLengthsA = zeros(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            ChLengthsA(TraceNo,ChNo) = ...
                length(AllFreqSersA{TraceNo,ChNo}.Mag);
            if ChLengthsA(TraceNo,ChNo) == 0
                ChLengthsA(TraceNo,ChNo) = 1;
            end
        end
    end
%     Train for length
    Results.Train = ...
        TrainLength(ChNamesT, logical(ChExistsT), ChLengthsT);
%     Get configurations for AnalyzeLength
    ChLenType = Conf.Analyze.Type.Text;
    ChLenConf = Conf.Analyze.Config;
%     Analyze for Length
    Results.Analyze = ...
        AnalyzeLength(ChLenType,ChLenConf,Results.Train,...
        ChNamesA,logical(ChExistsA),ChLengthsA);
end