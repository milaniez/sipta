RndStr = RandStream('mt19937ar','Seed','shuffle');
x = [ones(100,1);ones(100,1)*2];
y = zeros(200,100);
for i = 1:100
    y(:,i) = x(randperm(RndStr,200));
end
z = abs(fft(y));
z2 = zeros(200,100);
for i = 1:100
    z2(:,i)=z(:,i)*199/sum(z(2:200,i));
end
w = sort(z2,2);
Indexes = [101,32,65,16,3];
