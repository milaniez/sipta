function FreqSerOut = FreqSerSub(FreqSerIn1,FreqSerIn2)
    FreqSerOut = FreqSerMul(FreqSerIn1,FreqSerInv(FreqSerIn2));
end