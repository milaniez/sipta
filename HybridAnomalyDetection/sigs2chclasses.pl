use strict;
use warnings;

open (SIGINFO, "<$ARGV[0]\/sig_info.txt");
# my $Line = <SIGINFO>;
# my $SigCnt =~ /(*.?)/;
# $SigCnt = $1;
my $SigCnt = <SIGINFO>;
my $SigNo;
close (SIGINFO);

for ($SigNo = 0; $SigNo < $SigCnt; $SigNo++)
{
	open (SIGFILE, "<$ARGV[0]\/sig$SigNo\/sig.txt");
	open (CHCLASSINFO, ">$ARGV[0]\/sig$SigNo\/chclassinfo.txt");
	
	my @chClassFileList = ();
	
	my $Line = 0;
	my @SplittedLine = ();
	my $LineCnt = 0;
	my $IsFirstIteration = 1;
	
	while ($Line = <SIGFILE>)
	{
		@SplittedLine = split(/,/,$Line);
		if ($IsFirstIteration)
		{
			$IsFirstIteration = 0;
			for (my $ChCsNo = 0; $ChCsNo <= $#SplittedLine; $ChCsNo++)
			{
				mkdir "$ARGV[0]\/Sig$SigNo\/chClass$ChCsNo";
				open (my $chClassFile, ">$ARGV[0]\/Sig$SigNo\/chClass$ChCsNo\/chclass.txt");
				push (@chClassFileList, $chClassFile);
			}
			CHCLASSINFO -> print ("".@SplittedLine."");
		}
		for (my $ChCsNo = 0; $ChCsNo <= $#SplittedLine; $ChCsNo++)
		{
			$chClassFileList[$ChCsNo] -> print ("$SplittedLine[$ChCsNo]");
			if ($ChCsNo != $#SplittedLine)
			{
				$chClassFileList[$ChCsNo] -> print ("\n");
			}
		}
	}
	
	close SIGFILE;
	close CHCLASSINFO;
}