function Results = SpecLikeliClassifier...
        (ChNamesT,AllFreqSersT,ChNamesA,AllFreqSersA,Conf)
    [TraceCntT,ChCntT] = size(AllFreqSersT);
    [TraceCntA,ChCntA] = size(AllFreqSersA);
    ChHasLenT = zeros(TraceCntT,ChCntT);
    for TraceNo = 1:TraceCntT
        for ChNo = 1:ChCntT
            if ~isnan(AllFreqSersT{TraceNo,ChNo}.MinDist) && ...
                    AllFreqSersT{TraceNo,ChNo}.MinDist ~= 0
                ChHasLenT(TraceNo,ChNo) = 1;
            end
        end
    end
    ChHasLenA = zeros(TraceCntA,ChCntA);
    for TraceNo = 1:TraceCntA
        for ChNo = 1:ChCntA
            if ~isnan(AllFreqSersA{TraceNo,ChNo}.MinDist) && ...
                    AllFreqSersA{TraceNo,ChNo}.MinDist ~= 0
                ChHasLenA(TraceNo,ChNo) = 1;
            end
        end
    end
    
%     Prepare data for SpecLikeliTrain function
    Method = Conf.Method.Text;
    if strcmp(Method,'Envelop');
        AllEnvsT = cell(TraceCntT,ChCntT);
        PeakNeighRatio = str2num(Conf.Config.PeakNeighRatio.Text); %#ok<ST2NM>
        for TraceNo = 1:TraceCntT
            for ChNo = 1:ChCntT
                if ChHasLenT(TraceNo,ChNo) ~= 1
                    continue
                end
                AllEnvsT{TraceNo,ChNo} = ...
                    FreqSerNorm(EnvFreqSer(AllFreqSersT{TraceNo,ChNo},PeakNeighRatio));
            end
        end
    else
        error('Invalid Method')
    end
    AreaDiffType = Conf.General.AreaDiff.Text;
    AreaDiffLinkMethod = Conf.General.AreaDiffLinkMethod.Text;
    ClustSettings = Conf.General.Cluster;
    ClustType = ClustSettings.Type.Text;
    if strcmp(ClustType,'MaxCnt')
        MaxClustCnt = str2num(ClustSettings.MaxCnt.Text); %#ok<ST2NM>
    elseif strcmp(ClustType,'MaxCnt2')
        MaxClustCnt = str2num(ClustSettings.MaxCnt.Text); %#ok<ST2NM>
        MinLinkRatio = str2num(ClustSettings.LinkRatio.Text); %#ok<ST2NM>
    elseif ~strcmp(ClustType,'No')
        error('Invalid Cluster Typr');
    end
    AllEnvsClusts = cell(ChCntT,1);
    AllEnvAreaDiffTraceNos = cell(ChCntT,1);
    if ~strcmp(ClustType,'No')
        AllEnvAreaDiff = cell(ChCntT,1);
        AllEnvPDist = cell(ChCntT,1);
        AllEnvLinkage = cell(ChCntT,1);
        AreaDiffFcn = @(Idx1,Idx2,AreaDiff)(AreaDiff(Idx1,Idx2));
        for ChNo = 1:ChCntT
            EnvAreaDiffTraceNos = find((ChHasLenT(:,ChNo) == 1));
            AllEnvAreaDiffTraceNos{ChNo} = EnvAreaDiffTraceNos;
            EnvAreaDiffSize = length(EnvAreaDiffTraceNos);
            AllEnvAreaDiff{ChNo} = zeros(EnvAreaDiffSize);
            for Idx1 = 1:EnvAreaDiffSize
                for Idx2 = 1:EnvAreaDiffSize
                    TraceNo1 = EnvAreaDiffTraceNos(Idx1);
                    TraceNo2 = EnvAreaDiffTraceNos(Idx2);
                    Env1 = AllEnvsT{TraceNo1,ChNo};
                    Env2 = AllEnvsT{TraceNo2,ChNo};
                    if strcmp(AreaDiffType,'Sub')
                        AllEnvAreaDiff{ChNo}(Idx1,Idx2) = ...
                            FreqSerArea(FreqSerAbs(FreqSerSub(Env1,Env2)));
                    elseif strcmp(AreaDiffType,'Ratio')
                        AllEnvAreaDiff{ChNo}(Idx1,Idx2) = ...
                            FreqSerArea(FreqSerAbs2(FreqSerDiv(Env1,Env2))) - 1;
                    else
                        error('Invalid Option');
                    end
                end
            end
            if EnvAreaDiffSize > 1
                AllEnvPDist{ChNo} = pdist((1:EnvAreaDiffSize)',...
                    @(Idx1,Idx2) AreaDiffFcn(Idx1,Idx2,AllEnvAreaDiff{ChNo}));
                AllEnvLinkage{ChNo} = linkage(AllEnvPDist{ChNo},AreaDiffLinkMethod);
                AllEnvsClusts{ChNo} = cluster(AllEnvLinkage{ChNo},'maxclust',1:EnvAreaDiffSize);
            else
                AllEnvPDist{ChNo} = NaN;
                AllEnvLinkage{ChNo} = NaN;
                AllEnvsClusts{ChNo} = NaN;
            end
        end
    else
        for ChNo = 1:ChCntT
            EnvAreaDiffTraceNos = find((ChHasLenT(:,ChNo) == 1));
            AllEnvAreaDiffTraceNos{ChNo} = EnvAreaDiffTraceNos;
            EnvAreaDiffSize = length(EnvAreaDiffTraceNos);
            if EnvAreaDiffSize > 1
                AllEnvsClusts{ChNo} = ones(EnvAreaDiffSize,1);
            else
                AllEnvsClusts{ChNo} = NaN;
            end
        end
    end
    AllEnvsClust = cell(ChCntT,1);
    if strcmp(ClustType,'No');
        AllEnvsClust = AllEnvsClusts;
    elseif strcmp(ClustType,'AllTracesIncl')
        error ('Not implemented yet')
    elseif strcmp(ClustType,'MaxCnt')
        for ChNo = 1:ChCntT
            if isnan(AllEnvsClusts{ChNo})
                AllEnvsClust{ChNo} = nan;
            else
                ChMaxClustCnt = ...
                    min(floor(length(AllEnvAreaDiffTraceNos{ChNo})/2),MaxClustCnt);
                for ChClustCnt = ChMaxClustCnt:-1:1
                    ChClust = AllEnvsClusts{ChNo}(:,ChClustCnt);
                    ClustIsValid = true;
                    for ClustNo = 1:ChClustCnt
                        if sum(ChClust == ClustNo) < 2
                            ClustIsValid = false;
                            break
                        end
                    end
                    if ClustIsValid
                        ChMaxClustCnt = ChClustCnt;
                        break
                    end
                    if ChClustCnt == 1
                        ChMaxClustCnt = 1;
                        break
                    end
                end
                AllEnvsClust{ChNo} = AllEnvsClusts{ChNo}(:,ChMaxClustCnt);
            end
        end
    elseif strcmp(ClustType,'MaxCnt2')
        for ChNo = 1:ChCntT
            if isnan(AllEnvsClusts{ChNo})
                AllEnvsClust{ChNo} = nan;
            else
                ChMaxClustCnt = ...
                    min(floor(length(AllEnvAreaDiffTraceNos{ChNo})/2),MaxClustCnt);
                for ChClustCnt = ChMaxClustCnt:-1:1
                    ChClust = AllEnvsClusts{ChNo}(:,ChClustCnt);
                    ClustIsValid = true;
                    for ClustNo = 1:ChClustCnt
                        if sum(ChClust == ClustNo) < 2
                            ClustIsValid = false;
                            break
                        end
                    end
                    if ClustIsValid
                        ChMaxClustCnt = ChClustCnt;
                        break
                    end
                    if ChClustCnt == 1
                        ChMaxClustCnt = 1;
                        break
                    end
                end
                [LinkCnt,~] = size(AllEnvLinkage{ChNo});
                while ChMaxClustCnt > 1
                    if AllEnvLinkage{ChNo}(LinkCnt - ChMaxClustCnt + 2,3) > ...
                            MinLinkRatio*AllEnvLinkage{ChNo}(LinkCnt - ChMaxClustCnt + 1,3)
                        break;
                    end
                    ChMaxClustCnt = ChMaxClustCnt - 1;
                end
                AllEnvsClust{ChNo} = AllEnvsClusts{ChNo}(:,ChMaxClustCnt);
            end
        end
    end
%     Train for spectrum likelihood
    Results.Train = TrainSpecLikeli(ChNamesT, ChHasLenT,...
        AllEnvsClust, AllEnvAreaDiffTraceNos, AllEnvsT);
%     Prepare data for SpecLikeliAnalyze function
    Method = Conf.Method.Text;
    if strcmp(Method,'Envelop');
        AllEnvsA = cell(TraceCntA,ChCntA);
        PeakNeighRatio = str2num(Conf.Config.PeakNeighRatio.Text); %#ok<ST2NM>
        for TraceNo = 1:TraceCntA
            for ChNo = 1:ChCntA
                if ChHasLenA(TraceNo,ChNo) ~= 1
                    continue
                end
                AllEnvsA{TraceNo,ChNo} = ...
                    FreqSerNorm(EnvFreqSer(AllFreqSersA{TraceNo,ChNo},PeakNeighRatio));
            end
        end
    else
        error('Invalid Method')
    end
%     Get parameters for analyzing
    AnalyzeConf = Conf.Analyze.Config;
    AnalyzeType = Conf.Analyze.Type.Text;
%     Analyze for Spectrum Likelihood
    Results.Analyze = ...
        AnalyzeSpecLikeli(AnalyzeType,AnalyzeConf,...
        Results.Train,ChNamesA,logical(ChHasLenA),AllEnvsA);
end