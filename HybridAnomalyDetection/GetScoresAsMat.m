function Scores = GetScoresAsMat(Results)
    MethodCnt = length(Results);
    TraceCnt = length(Results{1}.Analyze);
    Scores = zeros(MethodCnt,TraceCnt);
    for MethodNo = 1:MethodCnt
        for TraceNo = 1:TraceCnt
            Scores(MethodNo,TraceNo) = Results{MethodNo}.Analyze{TraceNo}.Score;
        end
    end
end