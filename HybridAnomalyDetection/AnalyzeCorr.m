function AnalyzeResults = AnalyzeCorr(Type,TrainData,ChNames,ChHasLen,ChCorrs)
    if strcmp(Type,'MinStdRatioSum')
        [TraceCnt,~] = size(ChHasLen);
        AnalyzeResults = cell(TraceCnt,1);
        for TraceNo = 1:TraceCnt
            TraceChNames = ChNames(ChHasLen(TraceNo,:));
            TraceChCorrs = ChCorrs(TraceNo,ChHasLen(TraceNo,:));
            [~,TrainChIdx,AnalyzeChIdx] = intersect(TrainData.ChNames,TraceChNames);
            TrainAvg = TrainData.CorrAvg(TrainChIdx);
            TrainStd = TrainData.CorrStd(TrainChIdx);
            ValidChCorrs = TraceChCorrs(AnalyzeChIdx)';
            Score = sum(abs((ValidChCorrs - TrainAvg)./TrainStd))/length(ValidChCorrs);
            AnalyzeResults{TraceNo}.Score = Score;
        end
    else
        error('Invalid option');
    end
end