function TrainData = TrainLength(ChNames, ChExists, ChLengths)
    [TraceCnt,ChCnt] = size(ChExists);
    TrainData.TraceCnt = TraceCnt;
    TrainData.OrigChCnt = ChCnt;
    TrainData.OrigChNames = ChNames;
    ValidLengthData = false(1,ChCnt);
    TrainData.LengthAvg = zeros(1,ChCnt);
    TrainData.LengthStd = zeros(1,ChCnt);
    TrainData.LengthMin = zeros(1,ChCnt);
    TrainData.LengthMax = zeros(1,ChCnt);
    TrainData.Count = zeros(1,ChCnt);
    TrainData.Lengths = cell(1,ChCnt);
    TraceLen = zeros(TraceCnt,1);
    for TraceNo = 1:TraceCnt
        TraceLen(TraceNo) = sum(ChLengths(TraceNo,ChExists(TraceNo,:)));
    end
    for ChNo = 1:ChCnt
        if sum(ChExists(:,ChNo)) > 1
            ValidLengthData(ChNo) = true;
            ChLength = ChLengths(ChExists(:,ChNo),ChNo)./...
                TraceLen(ChExists(:,ChNo));
            TrainData.Lengths{ChNo} = ChLength;
            TrainData.Count(ChNo) = length(ChLength);
            TrainData.LengthAvg(ChNo) = mean(ChLength);
            TrainData.LengthStd(ChNo) = std(ChLength);
            TrainData.LengthMin(ChNo) = min(ChLength);
            TrainData.LengthMax(ChNo) = max(ChLength);
        end
    end
    TrainData.ChNames = ChNames(ValidLengthData);
    TrainData.ChCnt = length(TrainData.ChNames);
    TrainData.LengthAvg = TrainData.LengthAvg(ValidLengthData);
    TrainData.LengthStd = TrainData.LengthStd(ValidLengthData);
    TrainData.LengthMin = TrainData.LengthMin(ValidLengthData);
    TrainData.LengthMax = TrainData.LengthMax(ValidLengthData);
    TrainData.Count = TrainData.Count(ValidLengthData);
    TrainData.Lengths = TrainData.Lengths(ValidLengthData);
end