function AnalyzeResults = AnalyzeLength(Type,Config,TrainData,ChNames,ChExists,ChLengths)
    if strcmp(Type,'MinStdRatioSum')
        AnalyzeResults = AnalyzeLengthMinStdRatioSum...
            (Config,TrainData,ChNames,ChExists,ChLengths);
    elseif strcmp(Type,'DistSqr')
        
    else
        error('Invalid option');
    end
end