function TrainData = TrainExistence(ChNames, ChExists)
    [TraceCnt,ChCnt] = size(ChExists);
    TrainData.ChNames = ChNames;
    TrainData.TraceCnt = TraceCnt;
    TrainData.ChCnt = ChCnt;
    TrainData.ChExists = ChExists;
    TrainData.ExistenceProb = sum(ChExists)/TraceCnt;
end