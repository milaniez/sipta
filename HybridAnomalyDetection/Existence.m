function Results = Existence()
%     Train for length
    LengthTrainData = ...
        TrainLength(ChNamesT, logical(ChExistsT), ChLengthsT);
%     Get configurations for AnalyzeLength
    ChLengthType = Config.ChannelLength.Analyze.Type.Text;
    ChLengthConfig = Config.ChannelLength.Analyze.Config;
%     Analyze for Length
    LengthAnalyzeResults = ...
        AnalyzeLength(ChLengthType,ChLengthConfig,LengthTrainData,...
        ChNamesA,logical(ChExistsA),ChLengthsA);
end