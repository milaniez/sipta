function EnvedFreqSer = EnvFreqSer(FreqSer,PeakNeighRatio)
    RemFreqSer = FreqSer;
    DCVal = RemFreqSer.Mag(1);
    EnvedFreqSer.Freq = [0; 1];
    EnvedFreqSer.Mag = [DCVal; DCVal];
    EnvedFreqSer.MinDist = 1;
    NeighIdx = or((FreqSer.Freq <= PeakNeighRatio), ...
        (FreqSer.Freq >= (1 - PeakNeighRatio)));
    RemFreqSer.Mag(NeighIdx) = 0;
    while ~all(~(RemFreqSer.Mag))
        [PeakMag1,PeakIdx1] = max(RemFreqSer.Mag);
        PeakIdx2 = length(FreqSer.Mag) + 1 - PeakIdx1;
        PeakFreq1 = FreqSer.Freq(PeakIdx1);
        PeakFreq2 = FreqSer.Freq(PeakIdx2);
        PeakMag2 = PeakMag1;
        if PeakIdx1 == PeakIdx2
            PeakFreq2 = zeros(0,1);
            PeakMag2 = zeros(0,1);
        end
        EnvedFreqSer.Freq = [EnvedFreqSer.Freq; PeakFreq1; PeakFreq2];
        EnvedFreqSer.Mag = [EnvedFreqSer.Mag; PeakMag1; PeakMag2];
        [EnvedFreqSer.Freq, SortedIdx] = sort(EnvedFreqSer.Freq);
        EnvedFreqSer.Mag = EnvedFreqSer.Mag(SortedIdx);
        PeakFreq2 = FreqSer.Freq(PeakIdx2);
        NeighIdx = or(...
            and((FreqSer.Freq <= (PeakFreq1 + PeakNeighRatio)), ...
                (FreqSer.Freq >= (PeakFreq1 - PeakNeighRatio))),...
            and((FreqSer.Freq <= (PeakFreq2 + PeakNeighRatio)), ...
                (FreqSer.Freq >= (PeakFreq2 - PeakNeighRatio))));
        RemFreqSer.Mag(NeighIdx) = 0;
    end
    EnvDiff = FreqSerSub(EnvedFreqSer,FreqSer);
    while ~all(EnvDiff.Mag >= 0)
        [~,BottomIdx1] = min(EnvDiff.Mag);
        BottomFreq1 = EnvDiff.Freq(BottomIdx1);
        BottomMagFreqSerIdx1 = (FreqSer.Freq == BottomFreq1);
        BottomMagFreqSerIdx = or(BottomMagFreqSerIdx1,...
            flip(BottomMagFreqSerIdx1));
        BottomMag = FreqSer.Mag(BottomMagFreqSerIdx);
        BottomFreq = FreqSer.Freq(BottomMagFreqSerIdx);
        [EnvedFreqSer.Freq, SortedIdx] = ...
            sort([EnvedFreqSer.Freq; BottomFreq]);
        EnvedFreqSer.Mag = [EnvedFreqSer.Mag; BottomMag];
        EnvedFreqSer.Mag = EnvedFreqSer.Mag(SortedIdx);
        EnvDiff = FreqSerSub(EnvedFreqSer,FreqSer);
    end
end